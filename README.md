# **Workflow partitioning tool**

[TOC]

## Introduction

Scientific workflows have become a standardized way for scientists to represent a set of tasks to overcome or solve a certain problem. Usually these workflows consists of numerous amount of jobs that are both CPU heavy and I/O intensive that are executed using some kind of workflow management system either on clouds, grids, supercomputers, etc. Previously it has been shown, that using k-way partitioning algorithm to distribute a workflow's tasks between multiple machines in the cloud reduces the overall data communication and therefore lowering the cost of the bandwidth usage. In this thesis a framework was built in order automate this process - partition any workflow submitted by a scientist that is meant to be run on Pegasus workflow management system in the cloud with an ease. The framework provisions the instances in the cloud using CloudML, configures and installs all the software needed for the execution, runs and partitions the scientific workflow and finally shows the time estimation of the workflow, so that the user would have a rough guideline, how many resources one should provision in order to finish an experiment under a certain time-frame.

## Getting started

* Clone this repository!
* Run Application.java

## Running a workflow

1. Provide all the necessary settings in the landing page and click submit!  
![settings.PNG](https://bitbucket.org/repo/7EEK5xz/images/3428432289-settings.PNG)  
2. Optionally previous logs of a workflows run can be specified in the Estimator panel to see a workflows time-estimation. This shows a rough guideline how many cores to provision to run the workflow in a certain amount of time.  
![time.PNG](https://bitbucket.org/repo/7EEK5xz/images/2125544424-time.PNG)  
Note that after clicking the button analyze, in case of a deployment (clicking submit), the provided logs are used to partition the workflow, resulting in the skipping of the initial run.

3. The cluster is set-up.  
![deploy.PNG](https://bitbucket.org/repo/7EEK5xz/images/3808160859-deploy.PNG)  

4. The workflow is executed automatically.  
![workf.PNG](https://bitbucket.org/repo/7EEK5xz/images/4035143914-workf.PNG)  

5. If unhappy with the set-up, a new one can be selected and the partitioned workflow will be executed upon the new deployment.  
![newdepl.gif](https://bitbucket.org/repo/7EEK5xz/images/3443262914-newdepl.gif)  

## Fine-tuning

###User workflow script
The user workflow script that will be configured on each instance, should follow these strict guidelines:  
1. The necessary parts to run the workflow are set-up in the folder */home/ubuntu/Workflow*  
2. The **pegasus.properties** and the generated **dax.xml** are in the folder */home/ubuntu/Workflow/inputs*  
3. The plan to run the workflow is generated automatically and can be modified, if needed, in */home/ubuntu/Workflow/plan.sh*  
4. The execution sites are located in the /etc/hosts (MACHINE_0, MACHINE_1, ... , MACHINE_N). In the transformations catalog, each site should have the respective software to execute your workflow specified, for example:

```
tr mArchiveExec:3.3 {
    site MACHINE_0 {
        pfn "/home/ubuntu/Workflow/Montage/mArchiveExec"
        arch "x86_64"
        os "linux"
        type "INSTALLED"
        profile pegasus "clusters.size" "8"
    }
    ...
    ...
    site MACHINE_N {
        pfn "/home/ubuntu/Workflow/Montage/mArchiveExec"
        arch "x86_64"
        os "linux"
        type "INSTALLED"
        profile pegasus "clusters.size" "8"
    }


}

```


###Condor configuration
The tool configures Condor to use ports 9600-16000, so the associated security group should have these ports opened. (** verify if more ports are needed to be opened ** )

If changes are needed to be introduced to the condor configuration before the initial deployment, they can be done in *resources/scripts/install_software.sh*.
###Mule
The peer-to-peer communication between the machines in the cloud is achieved by using Mule. The data is aggressively cached on each machine in the location */tmp/mule* if the environmental variable "MULE_CACHE_DIR" is not specified.
###Pegasus
The tool installs Pegasus version *4.6.2* and adds a new handler named "MuleHandler" to */usr/bin/pegasus-transfer*. If the install script is modified to support a newer version of Pegasus, the pegasus-transfer must be modified accordingly in order to support Mule.