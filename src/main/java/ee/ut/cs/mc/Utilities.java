package ee.ut.cs.mc;

import org.apache.commons.io.IOUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.io.*;
import java.util.ArrayList;

/**
 * Utilities class for methods to be used across the project
 */
public final class Utilities {

    /**
     *
     * @param is the InputStream
     * @return the length of the inputstream
     * @throws IOException
     */
    public static long getStreamLength(InputStream is) throws IOException {
        int len;
        int size = 1024;
        byte[] buf;
        if (is instanceof ByteArrayInputStream) {
            size = is.available();
            buf = new byte[size];
            len = is.read(buf, 0, size);
        } else {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            buf = new byte[size];
            while ((len = is.read(buf, 0, size)) != -1)
                bos.write(buf, 0, len);
            buf = bos.toByteArray();
        }
        return buf.length;
    }

    /**
     * Iterates a list of BindingResults, and checks whether the fields gotten from the frontend
     * correspond to the format specified in the class that are tied to the BindingResult.
     * @param bindingResults - list of bindingresults
     * @return a String in the format of [field1,field2], that shows the fields with errors.
     */
    public static String getFieldErrors(BindingResult[] bindingResults) {
        String fieldsWithErrors = "[";
        for (BindingResult bindingResult : bindingResults) {
            for (FieldError error : bindingResult.getFieldErrors()) {
                fieldsWithErrors += error.getField()+",";
            }
        }
        fieldsWithErrors = fieldsWithErrors.replaceAll(",$",""); //remove last comma
        return fieldsWithErrors + "]";
    }

    /**
     * Writes an input stream to the file specified.
     * @param stream The input stream.
     * @param file The file to write the input stream to.
     * @throws IOException
     */
    public static void writeInputStreamToFile(InputStream stream, File file) throws IOException {
        File targetFile = file;
        OutputStream outStream = new FileOutputStream(targetFile);
        byte[] buffer = new byte[8 * 1024];
        int bytesRead;
        while ((bytesRead = stream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }
        IOUtils.closeQuietly(stream);
        IOUtils.closeQuietly(outStream);
    }
}
