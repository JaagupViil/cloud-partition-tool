package ee.ut.cs.mc.estimator;

import com.google.common.collect.Iterators;
import ee.ut.cs.mc.settings.EstimatorSettings;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * The class that estimates the time from the workflow logs.
 */
public class TimeEstimator {

    private DAX dax;
    private EstimatorSettings estimatorSettings;
    private LinkedHashMap<Integer, Double> realRunTimes;
    private final int[] cores = {2,4,8,12,16,20,24,28,32,36,40,44,48,52,56,60,64,68,72,76,80,84,88,92,96,100};

    public TimeEstimator(DAX dax, EstimatorSettings estimatorSettings) {
        this.dax = dax;
        this.estimatorSettings = estimatorSettings;
        this.realRunTimes = new LinkedHashMap<>();
        getNrOfCoresAndRuntimeFromLog();
    }

    /**
     * Populates the realRunTimes hashmap variable from the jobs log file, where
     * in the end of the file are the real execution times of the workflow and
     * the amount of cores the workflow
     * was executed on.
     */
    private void getNrOfCoresAndRuntimeFromLog() {
        String line = "";
        try (BufferedReader br = new BufferedReader(new FileReader(estimatorSettings.getJobsLogFileLocation()))) {
            while ((line = br.readLine()) != null) {
                if (line.contains("Total cores and execution time")) {
                    String[] splitLine = line.split(":");
                    String timeString = splitLine[2];
                    int cores = Integer.parseInt(splitLine[1]);
                    double time;
                    if (timeString.split(",").length > 1) {
                        time = parseTime(timeString.split(",")[0]) + parseTime(timeString.split(",")[1]);
                    } else {
                        time = parseTime(timeString);
                    }
                    realRunTimes.put(cores,time);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     *
     * @param time time string gotten from the log, in the format of : xhr, xmins or xsecs
     * @return the total seconds of the parsed time string
     */
    private int parseTime(String time) {
        int seconds = 0;
        if (time.contains("hrs") || time.contains("hr")) {
            seconds = Integer.parseInt(time.replaceAll("hrs|hr",""))*3600;
        } else if (time.contains("mins") || time.contains("min")) {
            seconds = Integer.parseInt(time.replaceAll("mins|min",""))*60;
        } else {
            seconds = Integer.parseInt(time.replaceAll("secs|sec",""));
        }
        return seconds;
    }


    /**
     * Iterates over the DAX jobs and using a top-down-approach, assigns
     * levels to the workflows jobs.
     * @return Returns a hashmap containing the different levels of the workflow and its jobs.
     */
    public HashMap<Integer, ArrayList<String>> getLevelsAndJobs() {
        HashMap<Integer, ArrayList<String>> jobsWithLevels = new HashMap<>();
        jobsWithLevels.put(0,new ArrayList<>());
        HashMap<String, String> idsAndNames = dax.getJobIDsAndNames();
        HashMap<String, HashMap<String, String>> connections = dax.getJobConnections();
        for (String id : idsAndNames.keySet()) {
            String nameAndId = idsAndNames.get(id).split(":")[0] + "_" + id;
            if (!connections.get(nameAndId).values().contains("from")) {
                ArrayList<String> arrayList = jobsWithLevels.get(0);
                arrayList.add(nameAndId);
                jobsWithLevels.put(0,arrayList);
            } else {
                int previousLevel = 0;
                for (String job : connections.get(nameAndId).keySet()) {
                    if (connections.get(nameAndId).get(job).equals("from")) {
                        int jobLevel = 0;
                        for (Integer key : jobsWithLevels.keySet()) {
                            if (jobsWithLevels.get(key).contains(job)) {
                                jobLevel = key;
                                break;
                            }
                        }
                        if (jobLevel > previousLevel) {
                            previousLevel = jobLevel;
                        }
                    }
                }
                ArrayList<String> arrayList;
                if (jobsWithLevels.get(previousLevel+1) == null) {
                    arrayList = new ArrayList<>();
                } else {
                    arrayList = jobsWithLevels.get(previousLevel+1);
                }
                arrayList.add(nameAndId);
                jobsWithLevels.put(previousLevel + 1,arrayList);
            }
        }

        return jobsWithLevels;
    }

    /**
     * Parses the job log file and assigns execution time to each job.
     * @return Returns a hashmap containing each job and its execution time.
     */
    public HashMap<String, Double> getJobsWithTime() {
        HashMap<String, Double> jobsWithMetric = new HashMap<>();
        HashMap<String, HashMap<String, String>> connections = dax.getJobConnections();
        String line = "";
        String splitBy = "\\s+";
        try (BufferedReader br = new BufferedReader(new FileReader(estimatorSettings.getJobsLogFileLocation()))) {
            while ((line = br.readLine()) != null) {
                String[] splitLine = line.split(splitBy);
                if (splitLine.length >= 15 && !line.contains("#") && !line.contains("CPU-Time")) {
                    String jobName = splitLine[0];
                    if (!splitLine[10].contains("-") && !splitLine[3].contains("-")) {
                        double time = Double.parseDouble(splitLine[10]);
                        if (connections.containsKey(jobName)) {
                            jobsWithMetric.put(jobName, time);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jobsWithMetric;
    }

    /**
     * Calculates the makespan/time-estimation of a workflow, given the number of cores to execute the workflow upon.
     * @param slots the amount of cores to calculate the time estimation from.
     * @param jobsWithTime a hashmap containing the jobs of the workflow and their time of execution.
     * @param levelsWithJobs the levels and their jobs of the workflow.
     * @return The time estimated it takes to execute a workflow on x cores.
     */
    public double calculateMakeSpan(int slots,HashMap<String, Double> jobsWithTime,HashMap<Integer, ArrayList<String>> levelsWithJobs) {
        double totalMakespan = 0;
        for (Integer level : levelsWithJobs.keySet()) {
            ArrayList<String> jobs = levelsWithJobs.get(level);
            double max_runtime = 0;
            double min_runtime = 0;
            double max_slots = jobs.size();
            for (String job : jobs) {
                if (jobsWithTime.containsKey(job)) {
                    max_runtime += jobsWithTime.get(job);
                    if (jobsWithTime.get(job) > min_runtime) {
                        min_runtime = jobsWithTime.get(job);
                    }
                }
            }
            double makespan = Math.max((max_runtime/(Math.min(slots,max_slots))),min_runtime);
            totalMakespan += makespan;
        }
        return totalMakespan;
    }

    /**
     * Calculates the runtime estimations for cores [2,..,100] based on the makespan algorithm.
     * @return
     */
    public LinkedHashMap<Integer,Double> calculateFromLogs() throws Exception {
        LinkedHashMap<Integer,Double> estimates = new LinkedHashMap<>();
        HashMap<String, Double> jobsWithTime = getJobsWithTime();
        HashMap<Integer, ArrayList<String>> levelsWithJobs = getLevelsAndJobs();
        for (int core : cores) {
            double runtime = calculateMakeSpan(core,jobsWithTime,levelsWithJobs);
            estimates.put(core, (double) Math.round(runtime));
        }

        return estimates;
    }

    /**
     * For every point x,y in the map, log(x) and log(y) are calculated.
     * @param estimates The points to apply the logarithm.
     * @return a map containing log(x) and log(y) for every x,y
     */
    private LinkedHashMap<Double,Double> calculateLogarithms(LinkedHashMap<Integer,Double> estimates) {
        LinkedHashMap<Double,Double> logEstimates = new LinkedHashMap<>();
        for (int key : estimates.keySet()) {
            double logX = Math.log(key);
            double logY = Math.log(estimates.get(key));
            logEstimates.put(logX,logY);
        }
        return logEstimates;
    }

    /**
     * Calculate the slope of logarithmic points.
     * @param logEstimates The logarithmic estimation points.
     * @return the slope
     */
    private double calculateSlope(LinkedHashMap<Double,Double> logEstimates) {
        double firstX = logEstimates.keySet().iterator().next();
        double lastX = Iterators.getLast(logEstimates.keySet().iterator());
        double firstY = logEstimates.get(firstX);
        double lastY = logEstimates.get(lastX);
        return (lastY-firstY)/(lastX-firstX);
    }

    /**
     * Calculate the intercept of logarithmic points.
     * @param logEstimates The logarithmic estimation points.
     * @return the intercept
     */
    private double calculateIntercept(LinkedHashMap<Double,Double> logEstimates) {
        double firstX = logEstimates.keySet().iterator().next();
        double firstY = logEstimates.get(firstX);
        return -(calculateSlope(logEstimates)*firstX)+firstY;
    }

    private double logOfBase(int base, double num) {
        return Math.log(num) / Math.log(base);
    }

    /**
     * Scale the estimates up by the last real runtime of the workflow.
     * @param estimates The estimates calculated by the makespan algorithm
     * @return The new scaled up estimates.
     */
    public LinkedHashMap<Integer,Double> scale(LinkedHashMap<Integer,Double> estimates) {
        LinkedHashMap<Double,Double> logEstimates = calculateLogarithms(estimates);
        LinkedHashMap<Integer,Double> scaledEstimates = new LinkedHashMap<>();
        int lastNrOfCores = Iterators.getLast(realRunTimes.keySet().iterator());
        double lastTotalRunTime = realRunTimes.get(lastNrOfCores);

        double intercept = calculateIntercept(logEstimates);
        double oldSlope = calculateSlope(logEstimates);
        double newSlope = logOfBase(lastNrOfCores,(lastTotalRunTime/Math.exp(intercept)));
        double newIntercept = Math.log(lastTotalRunTime/(Math.pow(lastNrOfCores,oldSlope)));

        System.out.println("New intercept "+newIntercept+" new slope "+newSlope);
        for (int core : estimates.keySet()) {
            double scaledY;
            if (oldSlope == 0) {
                scaledY = lastTotalRunTime;
            } else {
                if (newSlope <= 0) {
                    scaledY = Math.pow(core,newSlope) * Math.exp(intercept);
                } else {
                    scaledY = Math.pow(core,oldSlope) * Math.exp(newIntercept);
                }
            }
            scaledEstimates.put(core, Math.ceil(scaledY));
        }
        return scaledEstimates;
    }

    /**
     * Calculates a time estimation, in the form of y=x^a*e^b, based on the real runtimes of the workflow.
     * @return Returns the time estimation that is calculated based on the real workflow runtimes.
     */
    public LinkedHashMap<Integer,Double> calculateFromRealPoints() {
        System.out.println(realRunTimes);
        LinkedHashMap<Double,Double> logEstimates = calculateLogarithms(realRunTimes);
        LinkedHashMap<Integer,Double> scaledEstimates = new LinkedHashMap<>();
        double intercept = calculateIntercept(logEstimates);
        double slope = calculateSlope(logEstimates);
        for (int core : cores) {
            double scaledY = Math.pow(core,slope) * Math.exp(intercept);
            scaledEstimates.put(core, Math.ceil(scaledY));
        }
        return scaledEstimates;
    }

    /**
     * Converts each estimations time from minutes to seconds.
     * @param estimates The estimations to convert.
     * @return a new hashmap containing the estimations in seconds.
     */
    public LinkedHashMap<Integer,Double> convertSecondsToMinutes(LinkedHashMap<Integer,Double> estimates) {
        LinkedHashMap<Integer,Double> estimateInSeconds = new LinkedHashMap<>();
        for (int core : estimates.keySet()) {
            double timeInSeconds = estimates.get(core)/60.0;
            estimateInSeconds.put(core,(double) Math.round(timeInSeconds*100)/100);
        }
        return estimateInSeconds;
    }
}