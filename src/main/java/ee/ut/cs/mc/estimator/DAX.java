package ee.ut.cs.mc.estimator;

import ee.ut.cs.mc.settings.EstimatorSettings;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Builds a HashMap of the DAX file specified in the estimator settings
 */
public class DAX {

    /**
     * A hashmap in the format of "{job_ID01={job_ID02="to"},job_ID02={job_ID01="from"}}", specifying the connections of the jobs in the DAX.
     */
    private HashMap<String, HashMap<String,String>> jobConnections = new HashMap<>();
    private EstimatorSettings estimatorSettings;
    private Document dax;

    public DAX(EstimatorSettings estimatorSettings) throws Exception {
        this.estimatorSettings = estimatorSettings;
        loadDAX();
        jobConnections = buildConnections();
    }

    /**
     * Loads the DAX.xml that is specified in the estimator settings
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    private void loadDAX() throws ParserConfigurationException, IOException, SAXException {
        File fXmlFile = new File(estimatorSettings.getDaxFileLocation());
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        this.dax = dBuilder.parse(fXmlFile);
        this.dax.getDocumentElement().normalize();
    }

    /**
     * Traverses the jobs in the DAX and builds a hashmap containing the ID of the job, and its name+":"+index, e.g,
     * <br/>
     * {ID000001 = SOMEJOBNAME:1,ID000022 = SOMEOTHERJOBNAME:22}
     * @return
     */
    public HashMap<String,String> getJobIDsAndNames()  {
        LinkedHashMap<String, String> idsAndNames = new LinkedHashMap<>();
        int index = 1;
        NodeList nList = dax.getElementsByTagName("job");
        int length = nList.getLength();
        for (int i = 0; i < length; i++) {
            Node nNode = nList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                String id = eElement.getAttribute("id");
                String name = eElement.getAttribute("name");
                idsAndNames.put(id,name+":"+index);
                index++;
            }
        }
        return idsAndNames;
    }

    /**
     * Traverses over the DAX file and builds a hashmap, that contains the jobs of the workflow and
     * the jobs it is connected to, i.e., builds a hashmap in the format of:
     * <br/>
     * <b>{job_ID01={job_ID02="to"},job_ID02={job_ID01="from"}}</b>
     * <br/>
     * where for each job the preceding ("from") and succeeding ("to") jobs are listed.
     * @throws Exception
     */
    private HashMap<String, HashMap<String,String>> buildConnections() throws Exception {
        HashMap<String,String> idsAndNames = this.getJobIDsAndNames();
        HashMap<String, HashMap<String,String>> connections = new HashMap<>();
        NodeList childNodeList = dax.getElementsByTagName("child");
        int childNodesLength = childNodeList.getLength();
        for (int i = 0; i < childNodesLength; i++) {
            Node childNode = childNodeList.item(i);
            if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                Element childElement = (Element) childNode;
                String childId = childElement.getAttribute("ref");
                String childName = idsAndNames.get(childId).split(":")[0];
                String childNameAndId = childName+"_"+childId;
                if (!connections.containsKey(childNameAndId)) {
                    connections.put(childNameAndId,new HashMap<>());
                }
                NodeList parentNodeList = childElement.getElementsByTagName("parent");
                int parentNodesLength = parentNodeList.getLength();
                for (int j = 0; j < parentNodesLength; j++) {
                    Node parentNode = parentNodeList.item(j);
                    if (parentNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element parentElement = (Element) parentNode;
                        String parentId = parentElement.getAttribute("ref");
                        String parentName = idsAndNames.get(parentId).split(":")[0];
                        String parentNameAndId = parentName+"_"+parentId;
                        if (!connections.containsKey(parentNameAndId)) {
                            connections.put(parentNameAndId,new HashMap<>());
                        }
                        connections.get(parentNameAndId).put(childNameAndId,"to");
                        connections.get(childNameAndId).put(parentNameAndId,"from");
                    }
                }
            }
        }
        return connections;
    }

    public HashMap<String, HashMap<String, String>> getJobConnections() {
        return jobConnections;
    }

    public void setJobConnections(HashMap<String, HashMap<String, String>> jobConnections) {
        this.jobConnections = jobConnections;
    }

}
