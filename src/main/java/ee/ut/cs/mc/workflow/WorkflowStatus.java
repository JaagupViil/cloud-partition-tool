package ee.ut.cs.mc.workflow;

import ee.ut.cs.mc.ApplicationContext;
import ee.ut.cs.mc.settings.VMSettings;
import org.cloudml.connectors.Connector;
import org.cloudml.connectors.JCloudsConnector;
import org.cloudml.connectors.OpenStackConnector;

/**
 * The class that represents the status of the current workflow
 */
public class WorkflowStatus {

    private boolean startingExecution = false;
    public WorkflowStatus() {
    }

    /**
     * Gets the status of the workflow
     * @return the full status of the current workflow gotten from the command 'pegasus-status'
     */
    public String getStatus() {
        ApplicationContext context = ApplicationContext.getContext();
        Connector connector = context.getConnector();
        String workflowStatus = "";
        if (startingExecution) {
            workflowStatus = "Starting the execution!";
        } else {
            workflowStatus = "None";
            if (connector != null && context.getDeploymentStatus().isFinished()) {
                VMSettings vmSettings = context.getVmSettings();
                String vendor = context.getConnectionSettings().getVendor();
                String statusCommand = "pegasus-status -l \"$(ls -1dt /home/ubuntu/Workflow/submit/ubuntu/pegasus/*/* | head -n 1)\"";
                String mainNodeId = context.getDeployer().getVMInstances().get(0).getId();
                if (vendor.equals("openstack-nova")) {
                    workflowStatus = ((OpenStackConnector) connector).execCommandWithResponse(mainNodeId,
                            statusCommand, "ubuntu", vmSettings.getPrivateKeyLocation());
                } else {
                    workflowStatus = ((JCloudsConnector) connector).execCommandWithResponse(mainNodeId,
                            statusCommand, "ubuntu", vmSettings.getPrivateKeyLocation());
                }
            }
        }
        return workflowStatus;
    }


    public boolean isFinished() {
        return this.getStatus().contains("Success");
    }

    /**
     * Checks if the current workflow has been partitioned already.
     * @return a boolean showing whether the current workflow is partitioned or not.
     */
    public boolean isPartitioned() {
        ApplicationContext context = ApplicationContext.getContext();
        Connector connector = context.getConnector();
        boolean isPartitioned = false;
        if (connector != null) {
            VMSettings vmSettings = context.getVmSettings();
            String vendor = context.getConnectionSettings().getProvider().getName();
            String checkIfPartitionedCommand = "grep 'profile namespace=\"hints\" key=\"execution.site\"' /home/ubuntu/Workflow/inputs/partitioned_dax.xml | wc -l";
            String commandOutput;
            String mainNodeId = context.getDeployer().getVMInstances().get(0).getId();
            if (vendor.equals("openstack-nova")) {
                commandOutput = ((OpenStackConnector) connector).execCommandWithResponse(mainNodeId,
                        checkIfPartitionedCommand, "ubuntu", vmSettings.getPrivateKeyLocation());
            } else {
                commandOutput = ((JCloudsConnector) connector).execCommandWithResponse(mainNodeId,
                        checkIfPartitionedCommand, "ubuntu", vmSettings.getPrivateKeyLocation());
            }
            if (commandOutput.contains("No such file")) {
                isPartitioned = false;
            } else {
                int nrOfPartitionedJobs = Integer.parseInt(commandOutput.replace("\n", "").replace("\r", ""));
                isPartitioned = (nrOfPartitionedJobs > 0) ? true : false;
            }
        }
        return isPartitioned;
    }


    public boolean isStartingExecution() {
        return startingExecution;
    }

    public void setStartingExecution(boolean startingExecution) {
        this.startingExecution = startingExecution;
    }


}