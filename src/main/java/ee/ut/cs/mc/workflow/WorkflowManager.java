package ee.ut.cs.mc.workflow;

import ee.ut.cs.mc.ApplicationContext;
import ee.ut.cs.mc.settings.VMSettings;
import org.cloudml.connectors.Connector;
import org.cloudml.connectors.JCloudsConnector;
import org.cloudml.connectors.OpenStackConnector;
import org.cloudml.core.VMInstance;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Class responsible for the managing of the submitted workflow.
 */
public class WorkflowManager {

    private Connector connector;
    private VMSettings vmSettings;
    private String firstInstanceID;
    private ApplicationContext context;

    public WorkflowManager(Connector connector, VMSettings vmSettings, String firstInstanceID) {
        this.connector = connector;
        this.vmSettings = vmSettings;
        this.firstInstanceID = firstInstanceID;
        this.context = ApplicationContext.getContext();
    }

    public WorkflowManager() {
    }

    /**
     * Starts the execution of the workflow.
     * If the workflow has finished and is partitioned, runs the partitioned workflow, otherwise
     * executes the workflow normally.
     */
    public void startExecution() {
        WorkflowStatus workflowStatus = context.getWorkflowStatus();
        workflowStatus.setStartingExecution(true);
        cleanMuleCaches();
        if (workflowStatus.isFinished() && workflowStatus.isPartitioned()) {
            connector.execCommand(firstInstanceID, "cd /home/ubuntu/Workflow && bash partitioning_plan.sh",
                    "ubuntu", vmSettings.getPrivateKeyLocation());
        } else if (context.getEstimatorSettings().logsAreProvided()) {
            partitionWorkflow();
            connector.execCommand(firstInstanceID, "cd /home/ubuntu/Workflow && bash partitioning_plan.sh",
                    "ubuntu", vmSettings.getPrivateKeyLocation());
        } else {
            connector.execCommand(firstInstanceID, "cd /home/ubuntu/Workflow && bash plan.sh",
                    "ubuntu", vmSettings.getPrivateKeyLocation());
        }
        workflowStatus.setStartingExecution(false);
    }

    /**
     * Terminates the current execution of the workflow
     */
    public void terminateExecution() {
        connector.execCommand(firstInstanceID, "pegasus-remove \"$(ls -1dt /home/ubuntu/Workflow/submit/*/*/*/* | head -n 1)\"", "ubuntu", vmSettings.getPrivateKeyLocation());
    }

    /**
     * Restarts the execution of the current workflow, i.e., terminates it and starts it.
     */
    public void restartExecution() {
        terminateExecution();
        startExecution();
    }

    public InputStream getWorkflowTransferLog() {
        return getFileStream("/home/ubuntu/Workflow/transfers.txt");
    }

    public InputStream getWorkflowJobsLog() {
        return getFileStream("/home/ubuntu/Workflow/jobs.txt");
    }

    public InputStream getWorkflowDAX() {
        return getFileStream("/home/ubuntu/Workflow/inputs/dax.xml");
    }

    /**
     * Gets a file from the main node.
     * @param fileLocation The file location.
     * @return returns the InputStream of the file specified.
     */
    private InputStream getFileStream(String fileLocation) {
        Connector connector = context.getConnector();
        InputStream inputStream = null;
        String mainNodeId = context.getDeployer().getVMInstances().get(0).getId();
        if (connector != null) {
            VMSettings vmSettings = context.getVmSettings();
            String vendor = context.getConnectionSettings().getProvider().getName();
            if (vendor.equals("openstack-nova")) {
                inputStream = ((OpenStackConnector) connector).getFileStream(fileLocation,
                        mainNodeId,"ubuntu",vmSettings.getPrivateKeyLocation());
            } else {
                inputStream = ((JCloudsConnector) connector).getFileStream(fileLocation,
                        mainNodeId,"ubuntu",vmSettings.getPrivateKeyLocation());
            }
        }

        return inputStream;
    }

    /**
     * Partitions the workflow.
     * @return boolean showing if partitioning was successful.
     */
    public boolean partitionWorkflow() {
        String response = "";
        Connector connector = context.getConnector();
        if (connector != null) {
            VMSettings vmSettings = context.getVmSettings();
            String vendor = context.getConnectionSettings().getProvider().getName();
            if (vendor.equals("openstack-nova")) {
                response = ((OpenStackConnector) connector).execCommandWithResponse(firstInstanceID, "bash partition.sh",
                        "ubuntu", vmSettings.getPrivateKeyLocation());
            } else {
                response = ((JCloudsConnector) connector).execCommandWithResponse(firstInstanceID, "bash partition.sh",
                        "ubuntu", vmSettings.getPrivateKeyLocation());
            }
        }
        return response.contains("Partitioning successful!");
    }

    /**
     * Cleans the mule-rls and also the cache of each instance.
     */
    private void cleanMuleCaches() {
        ArrayList<VMInstance> vmInstances = context.getDeployer().getVMInstances();
        for (int i = 0; i < vmInstances.size(); i++) {
            String id = vmInstances.get(i).getId();
            connector.execCommand(id, "python /home/ubuntu/Mule/bin/mule rls_clear && python /home/ubuntu/Mule/bin/mule clear",
                    "ubuntu", vmSettings.getPrivateKeyLocation());
        }
    }


    @Override
    public String toString() {
        return "WorkflowManager{" +
                "connector=" + connector +
                ", vmSettings=" + vmSettings +
                ", firstInstanceID='" + firstInstanceID + '\'' +
                '}';
    }

}