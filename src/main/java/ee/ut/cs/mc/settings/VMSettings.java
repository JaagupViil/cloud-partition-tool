package ee.ut.cs.mc.settings;


import ee.ut.cs.mc.ApplicationContext;
import org.cloudml.core.ProvidedExecutionPlatform;
import org.cloudml.core.Provider;
import org.cloudml.core.VM;
import org.codehaus.jettison.json.JSONException;
import org.hibernate.validator.constraints.NotEmpty;
import org.jclouds.ec2.domain.InstanceType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * The class that represents a virtual machine settings - e.g, its storage size, ssh keys, image id's, etc.
 */
public class VMSettings implements Settings {

    @NotEmpty
    private String securityGroup;
    private ArrayList<String> instanceTypes;
    @NotEmpty
    private String sshKey;
    @NotEmpty
    private String privateKeyLocation;
    private String region;
    private int minRam = 512;
    private int minStorage = 10;
    private int minCores = 1;
    private int nrOfInstances = 2;
    private String publicAddressPattern = "172";
    private String instanceType;
    @NotEmpty
    private String imageId;
    private boolean skipInitialSetup;

    public VMSettings() {
        //Populate instance type values (for amazone instance selection) gotten from JClouds InstanceType interface
        instanceTypes = new ArrayList<>();
        for (Field field : new InstanceType().getClass().getDeclaredFields()) {
            try {
                instanceTypes.add(field.get(null).toString());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Create the VMSettings from an existing VM
     * @param vm the VM to create the VMsettings from
     */
    public VMSettings(VM vm) { //TODO: refactor to set methods
        super();
        this.securityGroup = vm.getSecurityGroup();
        this.sshKey = vm.getSshKey();
        this.privateKeyLocation = vm.getPrivateKey();
        setPrivateKeyLocation(vm.getPrivateKey());
        this.imageId = vm.getImageId();
        if (vm.getRegion() == null || vm.getRegion().equals("")) {
            this.setRegion(imageId.split("/")[0]);
        } else {
            this.setRegion(vm.getRegion());
        }
        this.minRam = vm.getMinRam();
        this.minStorage = vm.getMinStorage();
        this.minCores = vm.getMinCores();
        this.instanceType = vm.getProviderSpecificTypeName();
    }

    /**
     * Create a virtual machine based on the provider.
     * @param provider the provider/vendor of the current deployment.
     * @return a VM
     */
    public VM createVM(Provider provider) {
        VM virtualMachine = new VM("base-instance", provider);
        if (provider.getName().contains("aws")) {
            virtualMachine.setGroupName(this.securityGroup);
            virtualMachine.setLocation(this.region);
        } else {
            virtualMachine.setMinRam(this.minRam);
            virtualMachine.setMinCores(this.minCores);
        }
        virtualMachine.setProviderSpecificTypeName(this.instanceType);
        virtualMachine.setSecurityGroup(this.securityGroup);
        ProvidedExecutionPlatform vmOS = new ProvidedExecutionPlatform("vmOS");
        vmOS.setProperty("os", "Ubuntu");
        virtualMachine.setRegion(this.region);
        virtualMachine.setImageId(this.imageId);
        virtualMachine.getProvidedExecutionPlatforms().add(vmOS);
        virtualMachine.setName("ubuntu");
        virtualMachine.setIs64os(true);
        virtualMachine.setMinStorage(this.minStorage);
        virtualMachine.setSshKey(this.sshKey);
        virtualMachine.setPrivateKey(this.privateKeyLocation);


        return virtualMachine;
    }

    /**
     * Updates the hardware information of the settings depending on the vendor.
     * @param newVMSettings The new settings containing information about the ram,cores,storage or the instance type
     */
    public void updateHardwareSettings(VMSettings newVMSettings) {
        String vendor = ApplicationContext.getContext().getConnectionSettings().getVendor();
        if (vendor.equals("openstack-nova")) {
            this.setMinRam(newVMSettings.getMinRam());
            this.setMinCores(newVMSettings.getMinCores());
            this.setMinStorage(newVMSettings.getMinStorage());
        } else {
            this.setInstanceType(newVMSettings.instanceType);
        }

    }

    public String getSecurityGroup() {
        return securityGroup;
    }

    public void setSecurityGroup(String securityGroup) {
        this.securityGroup = securityGroup;
    }

    public String getSshKey() {
        return sshKey;
    }

    public void setSshKey(String sshKey) {
        this.sshKey = sshKey;
    }

    public String getPrivateKeyLocation() {
        return privateKeyLocation;
    }

    public void setPrivateKeyLocation(String privateKeyLocation) {
        this.privateKeyLocation = privateKeyLocation.replaceAll("[\\\\/]+", "\\\\\\\\");
    }

    public void setImageId(String imageId) {
        this.imageId = imageId.replaceAll("[\\\\/]+", "/");
        ;
    }

    public int getMinRam() {
        return minRam;
    }

    public void setMinRam(int minRam) {
        this.minRam = minRam;
    }

    public int getMinStorage() {
        return minStorage;
    }

    public void setMinStorage(int minStorage) {
        this.minStorage = minStorage;
    }

    public int getMinCores() {
        return minCores;
    }

    public void setMinCores(int minCores) {
        this.minCores = minCores;
    }

    public int getNrOfInstances() {
        return nrOfInstances;
    }

    public void setNrOfInstances(int nrOfInstances) {
        this.nrOfInstances = nrOfInstances;
    }

    public String getPublicAddressPattern() {
        return publicAddressPattern;
    }

    public void setPublicAddressPattern(String publicAddressPattern) {
        this.publicAddressPattern = publicAddressPattern;
    }

    public String getImageId() {
        return imageId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getInstanceType() {
        return instanceType;
    }

    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    public ArrayList<String> getInstanceTypes() {
        return instanceTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof VMSettings == false) {
            return false;
        }
        VMSettings vmSettings = (VMSettings) o;
        if (ApplicationContext.getContext().getConnectionSettings() == null) return false;
        String vendor = ApplicationContext.getContext().getConnectionSettings().getVendor();
        boolean isTypeSpecified = vmSettings.getInstanceType() != null || !vmSettings.getInstanceType().isEmpty();
        boolean objectsAreEqual;
        if (vendor.equals("openstack-nova") && !isTypeSpecified) {
            objectsAreEqual = vmSettings.getMinRam() == this.getMinRam() &&
                    vmSettings.getMinCores() == this.getMinCores() &&
                    vmSettings.getMinStorage() == this.getMinStorage();
        } else {
            objectsAreEqual = vmSettings.getInstanceType().equals(this.instanceType);
        }
        return objectsAreEqual;
    }

    @Override
    public int hashCode() {
        int code = this.minCores + this.minRam + this.minStorage;
        for (char c : instanceType.toLowerCase().toCharArray()) {
            code += (int) c;
        }
        return code;
    }


    @Override
    public String toString() {
        return "VMSettings{" +
                "securityGroup='" + securityGroup + '\'' +
                ", sshKey='" + sshKey + '\'' +
                ", privateKeyLocation='" + privateKeyLocation + '\'' +
                ", region='" + region + '\'' +
                ", minRam=" + minRam +
                ", minStorage=" + minStorage +
                ", minCores=" + minCores +
                ", nrOfInstances=" + nrOfInstances +
                ", publicAddressPattern='" + publicAddressPattern + '\'' +
                ", instanceType='" + instanceType + '\'' +
                ", imageId='" + imageId + '\'' +
//                ", amazonImageId='" + amazonImageId + '\'' +
                '}';
    }

    @Override
    public void readSettingsFromJson(ByteArrayInputStream inputStream) throws IOException, ParseException, JSONException {
        JSONObject json = (JSONObject) new JSONParser().parse(new InputStreamReader(inputStream));
        JSONObject vmSettingsJson = ((JSONObject) json.get("vmSettings"));
        if (vmSettingsJson != null) {
            try {
                vmSettingsJson = (JSONObject) new JSONParser().parse(vmSettingsJson.toJSONString().replaceAll("[\\\\/]+", "\\\\\\\\"));
                this.securityGroup = vmSettingsJson.get("securityGroup").toString();
                this.sshKey = vmSettingsJson.get("sshKey").toString();
                this.privateKeyLocation = vmSettingsJson.get("privateKeyLocation").toString();
                this.instanceType = vmSettingsJson.get("instanceType").toString();
                if (instanceType.equals("")) {
                    this.minRam = Integer.parseInt(vmSettingsJson.get("minRam").toString());
                    this.minCores = Integer.parseInt(vmSettingsJson.get("minCores").toString());
                }
                this.minStorage = Integer.parseInt(vmSettingsJson.get("minStorage").toString());
                this.nrOfInstances = Integer.parseInt(vmSettingsJson.get("nrOfInstances").toString());
                this.imageId = vmSettingsJson.get("imageId").toString().replace("\\", "/");
                this.skipInitialSetup = Boolean.parseBoolean(vmSettingsJson.get("skipInitialSetup").toString());
                this.region = vmSettingsJson.get("region").toString();
            } catch (Exception e) {
                throw new JSONException("Error parsing json!");
            }
        }
    }


    @Override
    public String getJsonString() {
        String json = "\"vmSettings\" : { \n" +
                "\"securityGroup\" : \"" + securityGroup + "\", \n" +
                "\"sshKey\" : \"" + sshKey + "\", \n" +
                "\"privateKeyLocation\": \"" + privateKeyLocation + "\", \n" +
                "\"minStorage\": \"" + minStorage + "\", \n";
        if (instanceType == null || instanceType.equals("")) {
            json += "\"minRam\": \"" + minRam + "\", \n" +
                    "\"minCores\": \"" + minCores + "\", \n";
        }
        json += "\"nrOfInstances\": \"" + nrOfInstances + "\", \n" +
                "\"imageId\": \"" + imageId + "\", \n" +
                "\"skipInitialSetup\": \"" + skipInitialSetup + "\", \n" +
                "\"region\": \"" + region + "\", \n" +
                "\"instanceType\": \"" + instanceType + "\" \n" +
                "}";
        return json;
    }

    public boolean skipInitialSetup() {
        return skipInitialSetup;
    }

    public boolean getSkipInitialSetup() {
        return skipInitialSetup;
    }

    public void setSkipInitialSetup(boolean skipInitialSetup) {
        this.skipInitialSetup = skipInitialSetup;
    }
}