package ee.ut.cs.mc.settings;

import org.cloudml.core.Provider;
import org.cloudml.core.credentials.Credentials;
import org.codehaus.jettison.json.JSONException;
import org.hibernate.validator.constraints.NotEmpty;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class representing all the settings related to the connection to the cloud, i.e, credentials, vendor name, etc.
 */
public class ConnectionSettings implements Settings{

    private MyCredentials amazonCredentials;
    private MyCredentials openstackCredentials;
    private String openstackEndpoint;
    private String openstackProjectName;
    @NotEmpty
    private String vendor;


    public ConnectionSettings() {
        this.openstackCredentials = new MyCredentials();
        this.amazonCredentials = new MyCredentials();
    }

    public Provider getProvider() {
        Provider provider = new Provider();
        provider.setName(vendor);
        if (vendor.equals("openstack-nova")) {
            MyCredentials copy = new MyCredentials(openstackProjectName +":"+openstackCredentials.getLogin(),openstackCredentials.getPassword());
            provider.setProperty("endPoint", openstackEndpoint);
            provider.setCredentials(copy);
        } else {
            provider.setCredentials(amazonCredentials);
        }
        return provider;
    }

    @Override
    public String toString() {
        return "ConnectionSettings{" +
                "amazonCredentials=" + amazonCredentials.getLogin() + ", " +amazonCredentials.getPassword() +
                ", openstackCredentials=" + openstackCredentials.getLogin() + ", " +openstackCredentials.getPassword() +
                ", openstackEndpoint='" + openstackEndpoint + '\'' +
                ", openstackProjectName='" + openstackProjectName + '\'' +
                ", vendor='" + vendor + '\'' +
                '}';
    }

    @Override
    public void readSettingsFromJson(ByteArrayInputStream inputStream) throws IOException, ParseException, JSONException {
        JSONObject json = (JSONObject) new JSONParser().parse(new InputStreamReader(inputStream));
        JSONObject conSettingsJson = ((JSONObject) json.get("connectionSettings"));
        if (conSettingsJson != null) {
            try {
                vendor = (conSettingsJson.get("vendor").toString());
                if (vendor.equals("openstack-nova")) {
                    openstackCredentials.setLogin(((JSONObject) conSettingsJson.get("openstackCredentials")).get("login").toString());
                    openstackEndpoint = (conSettingsJson.get("openstackEndpoint").toString());
                    openstackProjectName = (conSettingsJson.get("openstackProjectName").toString());
                } else {
                    amazonCredentials.setLogin(((JSONObject) conSettingsJson.get("amazonCredentials")).get("login").toString());
                }
            } catch (Exception e) {
                throw new JSONException("Error parsing json!");
            }
        }
    }

    @Override
    public String getJsonString() {
        String json = "\"connectionSettings\" : { \n" +
                "\"vendor\" : \""+vendor+"\", \n";
        if (vendor.equals("openstack-nova")) {
            json += "\"openstackCredentials\" : { \"login\" : \""+openstackCredentials.getLogin()+"\" }, \n" +
                    "\"openstackEndpoint\" : \""+ openstackEndpoint +"\", \n" +
                    "\"openstackProjectName\" : \""+ openstackProjectName +"\" \n";
        } else {
            json += "\"amazonCredentials\" : { \"login\" : \""+amazonCredentials.getLogin()+"\" } \n";
        }
        json += '}';
        return json;
    }

    public String getOpenstackEndpoint() {
        return openstackEndpoint;
    }

    public Credentials getCredentials() {
        if (vendor.equals("aws-ec2")) {
            return getAmazonCredentials();
        } else {
            return getOpenstackCredentials();
        }
    }

    public void setOpenstackEndpoint(String openstackEndpoint) {
        this.openstackEndpoint = openstackEndpoint.replaceAll("[\\\\]+","/");
    }

    public String getOpenstackProjectName() {
        return openstackProjectName;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public void setOpenstackProjectName(String openstackProjectName) {
        this.openstackProjectName = openstackProjectName;
    }

    public MyCredentials getOpenstackCredentials() {
        return openstackCredentials;
    }

    public void setOpenstackCredentials(MyCredentials openstackCredentials) {
        this.openstackCredentials = openstackCredentials;
    }

    public MyCredentials getAmazonCredentials() {
        return amazonCredentials;
    }

    public void setAmazonCredentials(MyCredentials amazonCredentials) {
        this.amazonCredentials = amazonCredentials;
    }

    class MyCredentials implements Credentials {

        private String login;
        private String password;

        public MyCredentials(String login, String password) {
            this.login = login;
            this.password = password;
        }
        public MyCredentials() {};

        @Override
        public String getLogin() {
            return login;
        }

        @Override
        public String getPassword() {
            return password;
        }

        @Override
        public void setLogin(String login) {
            this.login = login;
        }

        @Override
        public void setPassword(String pw) {
            this.password = pw;
        }
    }

}