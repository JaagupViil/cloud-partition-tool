package ee.ut.cs.mc.settings;

import org.codehaus.jettison.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * The class that resembles the workflows settings.
 */
public class WorkflowSettings implements Settings {
    private String customScriptLocation;

    public WorkflowSettings() {

    }
    public String getCustomScriptLocation() {
        return customScriptLocation;
    }

    public void setCustomScriptLocation(String customScriptLocation) {
        this.customScriptLocation = customScriptLocation.replaceAll("[\\\\/]+","\\\\\\\\");
    }

    public String getCustomScriptName() {
        String[] splitPath = customScriptLocation.split("\\\\");
        return splitPath[splitPath.length-1];
    }

    @Override
    public String toString() {
        return "WorkflowSettings{" +
                "customScriptLocation='" + customScriptLocation + '\'' +
                '}';
    }

    @Override
    public void readSettingsFromJson(ByteArrayInputStream inputStream) throws IOException, ParseException, JSONException {
        JSONObject json = (JSONObject) new JSONParser().parse(new InputStreamReader(inputStream));
        JSONObject workflowSettingsJson = ((JSONObject) json.get("workflowSettings"));
        if (workflowSettingsJson != null) {
            try {
                workflowSettingsJson = (JSONObject) new JSONParser().parse(workflowSettingsJson.toJSONString().replaceAll("[\\\\/]+", "\\\\\\\\"));
                this.customScriptLocation = workflowSettingsJson.get("customScriptLocation").toString();
            } catch (Exception e) {
                throw new JSONException("Error parsing json!");
            }
        }
    }

    @Override
    public String getJsonString() {
        return  "\"workflowSettings\" : { \n" +
                "\"customScriptLocation\" : \""+customScriptLocation+"\" \n" +
                "}";
    }

}