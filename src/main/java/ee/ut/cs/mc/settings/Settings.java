package ee.ut.cs.mc.settings;

import org.codehaus.jettison.json.JSONException;
import org.json.simple.parser.ParseException;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Interface for the settings
 */
public interface Settings {

    void readSettingsFromJson(ByteArrayInputStream inputStream) throws IOException, ParseException, JSONException;
    String getJsonString();
}