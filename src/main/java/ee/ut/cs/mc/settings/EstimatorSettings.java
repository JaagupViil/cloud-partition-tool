package ee.ut.cs.mc.settings;

import ee.ut.cs.mc.ApplicationContext;
import ee.ut.cs.mc.Utilities;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.URLDecoder;

/**
 * The class that represents all the settings related to the time estimator.
 */
public class EstimatorSettings implements Settings {

    private String transferLogFileLocation;
    private String jobsLogFileLocation;
    private String daxFileLocation;

    public EstimatorSettings() {
        this.transferLogFileLocation = "";
        this.jobsLogFileLocation = "";
        this.daxFileLocation = "";
    }

    /**
     * Constructs the estimator settings from various input streams
     * @param transferLog the transfer log input stream
     * @param jobsLog the jobs log input stream
     * @param daxFile the dax file input stream
     * @throws IOException
     */
    public EstimatorSettings(InputStream transferLog, InputStream jobsLog, InputStream daxFile) throws IOException {
        String path = URLDecoder.decode(ClassLoader.getSystemClassLoader().getResource(".").getPath(), "UTF-8");
        Utilities.writeInputStreamToFile(transferLog, new File(path + "/transfersLog.txt"));
        Utilities.writeInputStreamToFile(jobsLog, new File(path + "/jobsLog.txt"));
        Utilities.writeInputStreamToFile(daxFile, new File(path + "/daxFile.xml"));
        this.transferLogFileLocation = path + "/transfersLog.txt";
        this.jobsLogFileLocation = path + "/jobsLog.txt";
        this.daxFileLocation = path + "/daxFile.xml";


    }

    public String getTransferLogFileLocation() {
        return transferLogFileLocation;
    }

    public InputStream getTransferLogInputStream() {
        return ApplicationContext.getContext().getWorkflowManager().getWorkflowTransferLog();
    }

    public void setTransferLogFileLocation(String transferLogFileLocation) {
        this.transferLogFileLocation = transferLogFileLocation.replaceAll("[\\\\/]+","\\\\\\\\");;
    }

    public String getJobsLogFileLocation() {
        return jobsLogFileLocation;
    }

    public void setJobsLogFileLocation(String jobsLogFileLocation) {
        this.jobsLogFileLocation = jobsLogFileLocation.replaceAll("[\\\\/]+","\\\\\\\\");;
    }

    public String getDaxFileLocation() {
        return daxFileLocation;
    }

    public void setDaxFileLocation(String daxFileLocation) {
        this.daxFileLocation = daxFileLocation.replaceAll("[\\\\/]+","\\\\\\\\");;
    }

    public boolean logsAreProvided() {
        boolean notNull = transferLogFileLocation != null && jobsLogFileLocation != null && daxFileLocation != null;
        boolean notEmptyString = !transferLogFileLocation.equals("") && !jobsLogFileLocation.equals("") && !daxFileLocation.equals("");
        return notNull && notEmptyString;
    }

    @Override
    public void readSettingsFromJson(ByteArrayInputStream inputStream) throws IOException, ParseException, JSONException {
        JSONObject json = (JSONObject) new JSONParser().parse(new InputStreamReader(inputStream));
        JSONObject estimatorSettingsJson = ((JSONObject) json.get("estimatorSettings"));
        if (estimatorSettingsJson != null) {
            estimatorSettingsJson = (JSONObject) new JSONParser().parse(estimatorSettingsJson.toJSONString().replaceAll("[\\\\/]+", "\\\\\\\\"));
            try {
                this.setJobsLogFileLocation(estimatorSettingsJson.get("jobsLogFileLocation").toString());
                this.setDaxFileLocation(estimatorSettingsJson.get("daxFileLocation").toString());
                this.setTransferLogFileLocation(estimatorSettingsJson.get("transferLogFileLocation").toString());
            } catch (Exception e) {
                throw new JSONException("Error parsing json!");
            }
        }
    }

    @Override
    public String toString() {
        return "EstimatorSettings{" +
                "transferLogFileLocation='" + transferLogFileLocation + '\'' +
                ", jobsLogFileLocation='" + jobsLogFileLocation + '\'' +
                ", daxFileLocation='" + daxFileLocation + '\'' +
                '}';
    }

    @Override
    public String getJsonString() {
        return "\"estimatorSettings\" : { \n" +
                "\"transferLogFileLocation\" : \"" + transferLogFileLocation + "\", \n" +
                "\"jobsLogFileLocation\" : \"" + jobsLogFileLocation + "\", \n" +
                "\"daxFileLocation\": \"" + daxFileLocation + "\" \n" +
                "}";
    }
}