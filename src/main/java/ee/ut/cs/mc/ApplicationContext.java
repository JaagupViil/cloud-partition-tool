package ee.ut.cs.mc;

import ee.ut.cs.mc.deployment.Deployer;
import ee.ut.cs.mc.deployment.DeploymentStatus;
import ee.ut.cs.mc.settings.EstimatorSettings;
import ee.ut.cs.mc.settings.WorkflowSettings;
import ee.ut.cs.mc.workflow.WorkflowManager;
import ee.ut.cs.mc.workflow.WorkflowStatus;
import ee.ut.cs.mc.settings.ConnectionSettings;
import ee.ut.cs.mc.settings.VMSettings;
import org.cloudml.connectors.Connector;

/**
 * A class to share multiple variables and methods across the project.
 */
public class ApplicationContext {

    private final static ApplicationContext context = new ApplicationContext();

    private WorkflowManager workflowManager = new WorkflowManager();
    private WorkflowStatus workflowStatus = new WorkflowStatus();
    private DeploymentStatus deploymentStatus = new DeploymentStatus();
    private ConnectionSettings connectionSettings;
    private EstimatorSettings estimatorSettings;
    private WorkflowSettings workflowSettings;
    private VMSettings vmSettings;
    private Connector connector;
    private Deployer deployer;

    public ApplicationContext() {

    }

    public ConnectionSettings getConnectionSettings() {
        return connectionSettings;
    }

    public void setConnectionSettings(ConnectionSettings connectionSettings) {
        this.connectionSettings = connectionSettings;
    }

    public VMSettings getVmSettings() {
        return vmSettings;
    }

    public void setVmSettings(VMSettings vmSettings) {
        this.vmSettings = vmSettings;
    }

    public static ApplicationContext getContext() {
        return context;
    }

    public DeploymentStatus getDeploymentStatus() {
        return deploymentStatus;
    }

    public void setDeploymentStatus(DeploymentStatus deploymentStatus) {
        this.deploymentStatus = deploymentStatus;
    }

    public Connector getConnector() {
        return connector;
    }

    public void setConnector(Connector connector) {
        this.connector = connector;
    }

    public WorkflowStatus getWorkflowStatus() {
        return workflowStatus;
    }

    public void setWorkflowStatus(WorkflowStatus workflowStatus) {
        this.workflowStatus = workflowStatus;
    }

    public WorkflowManager getWorkflowManager() {
        return workflowManager;
    }

    public void setWorkflowManager(WorkflowManager workflowManager) {
        this.workflowManager = workflowManager;
    }

    public EstimatorSettings getEstimatorSettings() {
        return estimatorSettings;
    }

    public void setEstimatorSettings(EstimatorSettings estimatorSettings) {
        this.estimatorSettings = estimatorSettings;
    }

    public WorkflowSettings getWorkflowSettings() {
        return workflowSettings;
    }

    public void setWorkflowSettings(WorkflowSettings workflowSettings) {
        this.workflowSettings = workflowSettings;
    }

    public void setDeployer(Deployer deployer) {
        this.deployer = deployer;
    }

    public Deployer getDeployer() {
        return deployer;
    }

}
