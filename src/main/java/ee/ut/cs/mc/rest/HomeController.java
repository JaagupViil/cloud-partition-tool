package ee.ut.cs.mc.rest;

import ee.ut.cs.mc.ApplicationContext;
import ee.ut.cs.mc.Utilities;
import ee.ut.cs.mc.deployment.Deployer;
import ee.ut.cs.mc.settings.*;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.io.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Controller
public class HomeController {

    private ExecutorService executorService = Executors.newFixedThreadPool(5);
    private ConnectionSettings connectionSettings;
    private WorkflowSettings workflowSettings;
    private EstimatorSettings estimatorSettings;
    private VMSettings vmSettings;
    private Deployer deployer;
    private boolean loadingSettingsFromJson = false;

    @GetMapping(value = "/")
    public String index(Model model) throws Exception {
        boolean settingsUninitialized = connectionSettings == null && workflowSettings == null &&
                estimatorSettings == null && vmSettings == null;
        if (settingsUninitialized) {
            connectionSettings = new ConnectionSettings();
            workflowSettings = new WorkflowSettings();
            estimatorSettings = new EstimatorSettings();
            vmSettings = new VMSettings();
        }

        model.addAttribute("connectionSettings", connectionSettings);
        model.addAttribute("workflowSettings", workflowSettings);
        model.addAttribute("estimatorSettings", estimatorSettings);
        model.addAttribute("vmSettings", vmSettings);
        return "index";
    }

    @RequestMapping(value = "/submitSettings", headers = "content-type=multipart/*", method = RequestMethod.POST)
    public @ResponseStatus(value = HttpStatus.OK)
    void submitSettings(@Valid @ModelAttribute ConnectionSettings connectionSettings, BindingResult b1,
                        @Valid @ModelAttribute VMSettings vmSettings, BindingResult b2,
                        @Valid @ModelAttribute WorkflowSettings workflowSettings, BindingResult b3) throws Exception {

        String errorFields = Utilities.getFieldErrors(new BindingResult[]{b1,b2,b3});
        if (!errorFields.equals("[]")) {
            throw new ValidationException("Fields: "+errorFields+" should not be empty");
        }
        this.connectionSettings = connectionSettings;
        this.vmSettings = vmSettings;
        this.workflowSettings = workflowSettings;
        this.deployer = new Deployer(connectionSettings, vmSettings, workflowSettings);
        initAppContextSettingses();
        System.out.println(connectionSettings + " \n" + vmSettings + " \n" + workflowSettings + " \n" + ApplicationContext.getContext().getEstimatorSettings());
        System.out.println(ApplicationContext.getContext().getEstimatorSettings().logsAreProvided());
        deployer.deploy();
    }

    @RequestMapping(value = "/submitJSONSettingsFile", method = RequestMethod.POST)
    public @ResponseStatus(value = HttpStatus.OK) void submitJSONSettingsFile(@RequestParam("jsonFile") MultipartFile jsonFile) throws Exception {
        if (jsonFile.getBytes().length > 0) {
            loadingSettingsFromJson = true;
            connectionSettings.readSettingsFromJson(new ByteArrayInputStream(jsonFile.getBytes()));
            vmSettings.readSettingsFromJson(new ByteArrayInputStream(jsonFile.getBytes()));
            workflowSettings.readSettingsFromJson(new ByteArrayInputStream(jsonFile.getBytes()));
            estimatorSettings.readSettingsFromJson(new ByteArrayInputStream(jsonFile.getBytes()));
            initAppContextSettingses();
            System.out.println(connectionSettings+"\n"+vmSettings+"\n"+workflowSettings+"\n"+ estimatorSettings);
            loadingSettingsFromJson = false;
        } else {
            throw new FileNotFoundException("File is not found or is empty!");
        }
    }

    private void initAppContextSettingses() {
        ApplicationContext context = ApplicationContext.getContext();
        if (loadingSettingsFromJson || context.getEstimatorSettings() == null) {
            context.setEstimatorSettings(estimatorSettings);
        }
        context.setWorkflowSettings(workflowSettings);
        context.setVmSettings(vmSettings);
        context.setConnectionSettings(connectionSettings);
        context.setDeployer(deployer);
    }
}