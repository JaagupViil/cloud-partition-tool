package ee.ut.cs.mc.rest;

import ee.ut.cs.mc.ApplicationContext;
import ee.ut.cs.mc.estimator.DAX;
import ee.ut.cs.mc.estimator.TimeEstimator;
import ee.ut.cs.mc.settings.EstimatorSettings;
import ee.ut.cs.mc.workflow.WorkflowManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.LinkedHashMap;

@Controller
@RequestMapping("/estimator")
public class EstimatorController {

    EstimatorSettings estimatorSettings;

    @RequestMapping(value = "/estimateTimeFromUserLogs", headers = "content-type=application/json", method = RequestMethod.POST)
    public @ResponseBody HashMap[] estimateTimeFromUserLogs(@RequestBody EstimatorSettings estimatorSettings) throws Exception {
        this.estimatorSettings = estimatorSettings;
        ApplicationContext.getContext().setEstimatorSettings(this.estimatorSettings);
        System.out.println(ApplicationContext.getContext().getEstimatorSettings());
        return getAllEstimates();
    }

    @RequestMapping(value = "/estimateTimeFromInstanceLogs", method = RequestMethod.GET)
    public @ResponseBody HashMap[] estimateTimeFromInstanceLogs() throws Exception {
        System.out.println("Starting the estimation from instance logs");
        ApplicationContext context = ApplicationContext.getContext();
        if(context.getWorkflowStatus().isFinished()) {
            WorkflowManager manager = context.getWorkflowManager();
            estimatorSettings = new EstimatorSettings(manager.getWorkflowTransferLog(),manager.getWorkflowJobsLog(),manager.getWorkflowDAX());
            context.setEstimatorSettings(estimatorSettings);
        } else {
            throw new Exception("Workflow is not yet finished, can not estimate its time!");
        }
        return getAllEstimates();
    }

    private HashMap[] getAllEstimates() throws Exception {
        DAX connections = new DAX(this.estimatorSettings);
        TimeEstimator timeEstimator = new TimeEstimator(connections,this.estimatorSettings);
        LinkedHashMap<Integer,Double> estimates = timeEstimator.calculateFromLogs();
        LinkedHashMap<Integer,Double> scaledEstimates = timeEstimator.scale(estimates);
        LinkedHashMap<Integer,Double> realEstimates = timeEstimator.calculateFromRealPoints();
        for (int key : estimates.keySet()) {
            System.out.println(estimates.get(key));
        }
        System.out.println("----");
        for (int key : scaledEstimates.keySet()) {
            System.out.println(scaledEstimates.get(key));
        }
        System.out.println("----");
        for (int key : realEstimates.keySet()) {
            System.out.println(realEstimates.get(key));
        }
        return new HashMap[] {timeEstimator.convertSecondsToMinutes(estimates),timeEstimator.convertSecondsToMinutes(scaledEstimates),
                timeEstimator.convertSecondsToMinutes(realEstimates)};
    }

}