package ee.ut.cs.mc.rest;

import ee.ut.cs.mc.ApplicationContext;
import ee.ut.cs.mc.deployment.DeploymentStatus;
import ee.ut.cs.mc.workflow.WorkflowStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/status")
public class StatusController {

    private DeploymentStatus deploymentStatus = ApplicationContext.getContext().getDeploymentStatus();
    private WorkflowStatus workflowStatus = ApplicationContext.getContext().getWorkflowStatus();

    @GetMapping(value = "")
    public String statusPage(Model model)  {
        return "status";
    }

    @GetMapping(value = "/deploymentStatus")
    public @ResponseBody  DeploymentStatus getDeploymentStatus() {
        return deploymentStatus;
    }

    @GetMapping(value = "/workflowStatus")
    public @ResponseBody  String getWorkflowStatus() {
        return workflowStatus.getStatus();
    }

}
