package ee.ut.cs.mc.rest;

import ee.ut.cs.mc.ApplicationContext;
import ee.ut.cs.mc.Utilities;
import ee.ut.cs.mc.deployment.Deployer;
import ee.ut.cs.mc.deployment.DeploymentStatus;
import ee.ut.cs.mc.settings.EstimatorSettings;
import ee.ut.cs.mc.settings.ConnectionSettings;
import ee.ut.cs.mc.settings.VMSettings;
import ee.ut.cs.mc.settings.WorkflowSettings;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.ValidationException;

@Controller
@RequestMapping("/deployment")
public class DeploymentController {

    private Deployer deployer;

    private void initializeDeployer() {
        this.deployer = ApplicationContext.getContext().getDeployer();
    }

    @PostMapping(value = "/terminate")
    @ResponseStatus(value = HttpStatus.OK)
    public void terminate() throws Exception {
        initializeDeployer();
        if (deployer != null) {
            deployer.terminateDeployment();
        }

    }

    @PostMapping(value = "/restart")
    @ResponseStatus(value = HttpStatus.OK)
    public void restart() throws Exception {
        initializeDeployer();
        System.out.println("Starting to reset the deployment!" + deployer);
        if (deployer != null) {
            deployer.restartDeployment();
        }
    }

    @PostMapping(value = "/configure")
    @ResponseStatus(value = HttpStatus.OK)
    public void configure() throws Exception {
        initializeDeployer();
        if (deployer != null && ApplicationContext.getContext().getDeploymentStatus().getProgress() == 100) {
            deployer.configureInstances();
        }
    }


    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public @ResponseBody DeploymentStatus update(@RequestBody VMSettings vmSettings) {
        initializeDeployer();
        System.out.println("Starting to update the deployment!" + deployer);
        if (vmSettings != null) {
            deployer.update(vmSettings);
        }
        return ApplicationContext.getContext().getDeploymentStatus();
    }

    @RequestMapping(value = "/attach", method = RequestMethod.POST)
    public @ResponseBody DeploymentStatus attach(@RequestParam("jsonFileLocation") String jsonFileLocation,
                                                 @Valid @ModelAttribute ConnectionSettings connectionSettings, BindingResult b1) {
        initializeDeployer();
        String errorFields = Utilities.getFieldErrors(new BindingResult[]{b1});
        System.out.println(errorFields);
        if (!errorFields.equals("[]")) {
            throw new ValidationException("Fields: "+errorFields+" should not be empty");
        }
        ApplicationContext context = ApplicationContext.getContext();
        if (connectionSettings != null  && jsonFileLocation != null && !jsonFileLocation.isEmpty()) {
            VMSettings vmSettings = new VMSettings();
            WorkflowSettings workflowSettings = new WorkflowSettings();
            EstimatorSettings estimatorSettings = new EstimatorSettings();
            this.deployer = new Deployer(connectionSettings, vmSettings, workflowSettings);
            context.setEstimatorSettings(estimatorSettings);
            context.setWorkflowSettings(workflowSettings);
            context.setVmSettings(vmSettings);
            context.setConnectionSettings(connectionSettings);
            context.setDeployer(deployer);
            deployer.attach(jsonFileLocation.replaceAll("[\\\\/]+","\\\\\\\\"));
        }
        return context.getDeploymentStatus();
    }

}