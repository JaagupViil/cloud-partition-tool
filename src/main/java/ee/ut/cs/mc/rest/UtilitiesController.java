package ee.ut.cs.mc.rest;

import ee.ut.cs.mc.ApplicationContext;
import ee.ut.cs.mc.deployment.Deployer;
import ee.ut.cs.mc.settings.ConnectionSettings;
import ee.ut.cs.mc.settings.Settings;
import ee.ut.cs.mc.settings.VMSettings;
import ee.ut.cs.mc.workflow.WorkflowManager;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.jclouds.openstack.nova.v2_0.domain.Flavor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;

@Controller
public class UtilitiesController {


    @RequestMapping(value = "/download/{name}", method = RequestMethod.GET)
    public @ResponseStatus(value = HttpStatus.OK) void downloadFile(@PathVariable String name, HttpServletResponse response) throws Exception {
        WorkflowManager workflowManager = workflowManager = ApplicationContext.getContext().getWorkflowManager();
        String filePathToBeServed = System.getProperty("user.dir");
        String fileName = "";
        InputStream inputStream = null;
        switch (name) {
            case "transfersLog":
                fileName = "transfers.txt";
                inputStream = workflowManager.getWorkflowTransferLog();
                break;
            case "jobsLog":
                fileName = "jobs.txt";
                inputStream = workflowManager.getWorkflowJobsLog();
                break;
            case "DAX":
                fileName = "dax.xml";
                inputStream = workflowManager.getWorkflowDAX();
                break;
            case "Json":
                ApplicationContext c = ApplicationContext.getContext();
                Settings[] settings = {c.getConnectionSettings(),c.getVmSettings(),c.getWorkflowSettings(),c.getEstimatorSettings()};
                for (Settings setting : settings) {
                    if (setting == null) {
                        throw new Exception("Settings can be saved after starting the deployment!");
                    }
                }
                String str;
                try {
                    str = constructJSONString(settings);
                    inputStream = new ByteArrayInputStream(str.getBytes());
                } catch (Exception e) {
                    throw new Exception("Unable to save settings!");
                }
                fileName = "settings.json";
                break;
            default:
                break;
        }
        filePathToBeServed += "/"+ fileName;
        File fileToDownload = new File(filePathToBeServed);
        response.setContentType("application/force-download");
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        IOUtils.copy(inputStream, response.getOutputStream());
        response.flushBuffer();
        inputStream.close();
    }

    @RequestMapping(value = "/openstack/flavors", method = RequestMethod.GET)
    public @ResponseBody ArrayList<Flavor> getOpenstackFlavors() throws Exception {
        Deployer deployer = ApplicationContext.getContext().getDeployer();
        if (deployer ==  null) {
            throw new IllegalAccessException("Can not get openstack-flavors!");
        }
        return deployer.getOpenStackFlavors();
    }

    @RequestMapping(value = "/vendor", method = RequestMethod.GET)
    public @ResponseBody String getProviderName() throws Exception {
        ConnectionSettings connectionSettings = ApplicationContext.getContext().getConnectionSettings();
        if (connectionSettings ==  null) {
            throw new IllegalAccessException("Can not get provider");
        }
        return connectionSettings.getVendor();
    }

    @RequestMapping(value = "/region", method = RequestMethod.GET)
    public @ResponseBody String getRegion() throws Exception {
        VMSettings vmSettings = ApplicationContext.getContext().getVmSettings();
        if (vmSettings ==  null) {
            throw new IllegalAccessException("Can not get region");
        }
        return vmSettings.getRegion();
    }

    private String constructJSONString(Settings[] settings) {
        String jsonString = "{\n";
        for (int i = 0; i < settings.length; i++) {
            jsonString += settings[i].getJsonString();
            if (i != settings.length-1) {
                jsonString += ",";
            }
            jsonString += '\n';
        }
        jsonString += "}";

        return jsonString;
    }
}