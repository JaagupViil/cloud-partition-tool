package ee.ut.cs.mc.rest;

import ee.ut.cs.mc.ApplicationContext;
import ee.ut.cs.mc.settings.Settings;
import ee.ut.cs.mc.workflow.WorkflowManager;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

@Controller
@RequestMapping("/workflow")
public class WorkflowController {

    private WorkflowManager workflowManager;

    private void initializeWorkflowManager() {
        workflowManager = ApplicationContext.getContext().getWorkflowManager();
    }

    @GetMapping(value = "/start")
    @ResponseStatus(value = HttpStatus.OK)
    void startWorkflowExecution() {
        initializeWorkflowManager();
        if (workflowManager != null) {
            workflowManager.startExecution();
        }
    }

    @GetMapping(value = "/terminate")
    @ResponseStatus(value = HttpStatus.OK)
    void stopWorkflowExecution() {
        initializeWorkflowManager();
        if (workflowManager != null) {
            workflowManager.terminateExecution();
        }
    }

    @GetMapping(value = "/restart")
    @ResponseStatus(value = HttpStatus.OK)
    void restartWorkflowExecution() {
        initializeWorkflowManager();
        if (workflowManager != null) {
            workflowManager.restartExecution();
        }
    }

    @GetMapping(value = "/partition")
    public @ResponseBody  boolean partitionWorkflow() {
        initializeWorkflowManager();
        if (workflowManager != null && ApplicationContext.getContext().getWorkflowStatus().isFinished()) {
            return workflowManager.partitionWorkflow();
        }
        return false;
    }

}