package ee.ut.cs.mc.deployment;

/**
 * Class that represents the status of the current deployment.
 */
public class DeploymentStatus {

    private String status = "None";
    private int progress = 0;

    public DeploymentStatus() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public void setStatusAndProgress(String status, int progress) {
        this.status = status;
        this.progress = progress;
    }

    public boolean isFinished() {
        return this.getStatus().contains("Deployment finished!");
    }
}
