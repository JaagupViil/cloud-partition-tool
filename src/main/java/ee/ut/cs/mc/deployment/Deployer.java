package ee.ut.cs.mc.deployment;


import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.inject.Module;
import ee.ut.cs.mc.Application;
import ee.ut.cs.mc.ApplicationContext;
import ee.ut.cs.mc.Utilities;
import ee.ut.cs.mc.settings.EstimatorSettings;
import ee.ut.cs.mc.settings.ConnectionSettings;
import ee.ut.cs.mc.settings.VMSettings;
import ee.ut.cs.mc.settings.WorkflowSettings;
import ee.ut.cs.mc.workflow.WorkflowManager;
import org.cloudml.codecs.JsonCodec;
import org.cloudml.codecs.commons.Codec;
import org.cloudml.connectors.Connector;
import org.cloudml.connectors.ConnectorFactory;
import org.cloudml.connectors.JCloudsConnector;
import org.cloudml.connectors.OpenStackConnector;
import org.cloudml.core.*;
import org.cloudml.core.credentials.FileCredentials;
import org.cloudml.deployer.CloudAppDeployer;
import org.cloudml.facade.CloudML;
import org.cloudml.facade.Factory;
import org.cloudml.facade.commands.LoadDeployment;
import org.cloudml.mrt.Coordinator;
import org.cloudml.mrt.SimpleModelRepo;
import org.jclouds.ContextBuilder;
import org.jclouds.compute.ComputeService;
import org.jclouds.compute.ComputeServiceContext;
import org.jclouds.ec2.features.SubnetApi;
import org.jclouds.io.Payloads;
import org.jclouds.io.payloads.InputStreamPayload;
import org.jclouds.logging.config.NullLoggingModule;
import org.jclouds.openstack.nova.v2_0.NovaApi;
import org.jclouds.openstack.nova.v2_0.compute.NovaComputeService;
import org.jclouds.openstack.nova.v2_0.domain.Flavor;
import org.jclouds.openstack.nova.v2_0.features.FlavorApi;
import org.jclouds.openstack.nova.v2_0.features.ServerApi;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

import static org.jclouds.Constants.*;

/**
 * The class responsible for the deployment & configuration of the specified instances.
 */
public class Deployer {

    private VMSettings vmSettings;
    private WorkflowSettings workflowSettings;
    private ConnectionSettings connectionSettings;
    private Deployment deployment;
    private DeploymentStatus deploymentStatus;
    private Connector connector;
    private ApplicationContext context;
    private long startTime;
    private ExecutorService executorService = Executors.newFixedThreadPool(10);

    public Deployer(ConnectionSettings connectionSettings, VMSettings vmSettings, WorkflowSettings workflowSettings) {
        this.context = ApplicationContext.getContext();
        this.connectionSettings = connectionSettings;
        this.workflowSettings = workflowSettings;
        this.vmSettings = vmSettings;
        this.connector = ConnectorFactory.createIaaSConnector(connectionSettings.getProvider());
        this.deployment = new Deployment();
        this.deploymentStatus = context.getDeploymentStatus();
        this.startTime = System.currentTimeMillis();
        context.setConnector(connector);
    }


    private Runnable attachDeployment(String deploymentJsonLocation) {
        Runnable r = () -> {
            deploymentStatus.setStatusAndProgress("Starting to attach the deployment", 10);
            File file = new File(deploymentJsonLocation);

            if (file.exists()) {
                CloudML cml = Factory.getInstance().getCloudML();
                cml.fireAndWait(new LoadDeployment(deploymentJsonLocation));
                deployment = cml.getDeploymentModel();
            } else {
                deploymentStatus.setStatus("Unable to attach deployment, deployment json doesn't exist!");
                return;
            }

            ArrayList<VMInstance> vmInstances = getVMInstances();
            vmSettings = new VMSettings(getVM());

            Collections.sort( //Sort instances by the last numbers in their name
                    vmInstances,
                    (vm1, vm2) -> vm1.getName().substring(vm1.getName().lastIndexOf('-') + 1).
                            compareTo(vm2.getName().substring(vm2.getName().lastIndexOf('-') + 1))
            );
            deployment.getComponentInstances().clear();
            deployment.getProviders().clear();
            VM vm = vmSettings.createVM(connectionSettings.getProvider());
            vmInstances.forEach((vmInstance) -> {
                vmInstance.setType(vm);
                deployment.getComponentInstances().add(vmInstance);
            });
            deployment.getProviders().add(connectionSettings.getProvider());
            vmSettings.setNrOfInstances(vmInstances.size());
            updateAddresses(getVMInstances());
            context.setVmSettings(vmSettings);
            deploymentStatus.setStatusAndProgress("Deployment finished!", 100);
            WorkflowManager workflowManager = new WorkflowManager(connector, vmSettings, vmInstances.get(0).getId());
            context.setWorkflowManager(workflowManager);
        };
        return r;
    }

    private Runnable updateDeployment(VMSettings newVMSettings){
        Runnable r = () ->{
            deploymentStatus.setStatusAndProgress("Updating deployment!", 10);
            //Same machine as before, just add or remove instances from the current deployment and configure them
            System.out.println(newVMSettings + " \n" + vmSettings);

            ArrayList<VMInstance> vmInstances = getVMInstances();
            if (vmSettings.equals(newVMSettings)) {
                CloudAppDeployer cloudAppDeployer = createCloudMLDeployer(deployment);
                if (newVMSettings.getNrOfInstances() < vmSettings.getNrOfInstances()) {
                    int amountToRemove = vmSettings.getNrOfInstances() - newVMSettings.getNrOfInstances();
                    deployment.getComponentInstances().clear();
                    ArrayList<VMInstance> newVMInstances = new ArrayList<>();
                    for (int i = 0; i < vmInstances.size(); i++) {
                        if (i < (vmInstances.size() - amountToRemove)) {
                            deployment.getComponentInstances().add(vmInstances.get(i));
                            newVMInstances.add(vmInstances.get(i));
                        } else {
                            connector.destroyVM(vmInstances.get(i).getId());
                        }
                    }

                } else if (newVMSettings.getNrOfInstances() > vmSettings.getNrOfInstances()) {
                    int amountToAdd = newVMSettings.getNrOfInstances() - vmSettings.getNrOfInstances();
                    String login = this.connectionSettings.getCredentials().getLogin();
                    String lastNodesName = vmInstances.get(vmInstances.size() - 1).getName();
                    int last_instance_nr = Integer.parseInt(lastNodesName.split("-")[lastNodesName.split("-").length - 1]);
                    VM vm = vmSettings.createVM(connectionSettings.getProvider());
                    for (int i = last_instance_nr; i < amountToAdd + last_instance_nr; i++) {
                        VMInstance vmInstance = new VMInstance(login + "-" + startTime + "-" + (i + 1), vm);
                        deployment.getComponentInstances().add(vmInstance);
                    }
                }

                cloudAppDeployer.deploy(deployment);
                deploymentStatus.setStatusAndProgress("Starting configurations", 70);
                writeDeploymentToJson();
                configureInstances();
                deploymentStatus.setStatusAndProgress("Deployment finished!", 100);
                vmSettings.setNrOfInstances(getVMInstances().size());
            } else {
                vmSettings.updateHardwareSettings(newVMSettings);
                try {
                    terminateDeployment();
                    deployment = new Deployment();
                    vmSettings.setSkipInitialSetup(true);
                    deploy();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };
        return r;
    }

    private Runnable setUpDeployment() {

        Runnable r = () -> {
            deploymentStatus.setStatusAndProgress("Started deployment", 5);

            ArrayList<VMInstance> vmInstances = new ArrayList<>();
            VM vm = vmSettings.createVM(connectionSettings.getProvider());
            String login = connectionSettings.getCredentials().getLogin();

            deployment.setName(login + "-" + startTime + "-" + "deployment");
            deployment.getProviders().add(connectionSettings.getProvider());
            deployment.getComponents().add(vm);
            CloudAppDeployer cloudAppDeployer = createCloudMLDeployer(deployment);

            for (int i = 0; i < this.vmSettings.getNrOfInstances(); i++) {
                vmInstances.add(new VMInstance(login + "-" + startTime + "-" + i, vm));
            }
//            context.setVmInstances(vmInstances);
            if (vmSettings.skipInitialSetup()) {
                // Setup instances from snapshot
                deploymentStatus.setStatusAndProgress("Spawning instances from the image.", 20);
                for (VMInstance vmInstance : vmInstances) {
                    deployment.getComponentInstances().add(vmInstance);
                }
                cloudAppDeployer.deploy(deployment);
                uploadLogs(getVMInstances().get(0).getId());
            } else {
                // Setup first instance, snapshot it, and spawn the rest of the machines
                boolean successfulSetup = setUpMainInstance(cloudAppDeployer, vmInstances.get(0));
                if (!successfulSetup) {
                    return;
                }
                String image_id = createImage(vmInstances.get(0), 3);
                if (image_id.equals("")) {
                    deploymentStatus.setStatus("Problem snapshotting!");
                    return;
                }
                if (!image_id.contains("/")) {
                    vm.setImageId(vm.getRegion()+"/"+image_id);
                } else {
                    vm.setImageId(image_id);
                }
                deploymentStatus.setStatusAndProgress("Spawning rest of the instances.", 60);
                Deployment clone = deployment.clone();
                for (VMInstance vmInstance : vmInstances) {
                    clone.getComponentInstances().add(vmInstance);
                }
                cloudAppDeployer.deploy(clone);
            }
            deploymentStatus.setStatusAndProgress("Starting configurations", 70);

            vmSettings.setImageId(vm.getImageId());
            vmSettings.setSkipInitialSetup(true);
            vmSettings.setNrOfInstances(getVMInstances().size());
            writeDeploymentToJson();
            configureInstances();
            deploymentStatus.setStatusAndProgress("Deployment finished!", 100);

            WorkflowManager workflowManager = new WorkflowManager(connector, vmSettings, getVMInstances().get(0).getId());
            context.setWorkflowManager(workflowManager);
            if (!ApplicationContext.getContext().getWorkflowStatus().isPartitioned()) {
                workflowManager.startExecution();
            }
        };
        return r;

    }

    public void deploy() {
        submitDeploymenRunnableToExecutor(setUpDeployment(),"Problem during deployment!");
    }
    public void attach(String deploymentJsonLocation) {
        submitDeploymenRunnableToExecutor(attachDeployment(deploymentJsonLocation),"Problem attaching the deployment!");
    }
    public void update(VMSettings newVMSettings) {
        submitDeploymenRunnableToExecutor(updateDeployment(newVMSettings),"Problem updating the deployment!");
    }

    private void submitDeploymenRunnableToExecutor(Runnable runnable, String message) {
        executorService.submit(() -> {
            try {
                runnable.run();
            } catch (Exception e) {
                e.printStackTrace();
                if (e.getMessage().contains("groupId")) {
                    context.getDeploymentStatus().setStatus("Problem with security group" + " " + e.getMessage());
                } else {
                    context.getDeploymentStatus().setStatus(message + " " + e.getMessage());
                }
                executorService.shutdownNow();
                executorService = Executors.newFixedThreadPool(10);
            }
        });
    }


    /**
     * Creates the CloudAppDeployer from the deployment specified
     * @param deployment the deployment to create the CloudML CloudAppDeployer from.
     * @return returns the created CloudAppDeployer
     */
    private CloudAppDeployer createCloudMLDeployer(Deployment deployment) {
        Coordinator coordinator = new Coordinator();
        coordinator.setModelRepo(new SimpleModelRepo(deployment));
        CloudAppDeployer cloudAppDeployer = new CloudAppDeployer();
        cloudAppDeployer.setCoordinator(coordinator);
        return cloudAppDeployer;
    }

    /**
     * Writes the current deployment to a file called "deployment.json", which can be later used
     * to tap back to the running system.
     */
    private void writeDeploymentToJson() {
        Codec jsonCodec = new JsonCodec();
        OutputStream streamResult = null;
        try {
            streamResult = new FileOutputStream("deployment.json");
            FileCredentials credentials = new FileCredentials();
            credentials.setLogin(connectionSettings.getCredentials().getLogin());
            credentials.setPassword(connectionSettings.getCredentials().getPassword());
            deployment.getProviders().iterator().next().setCredentials(credentials);
            VM vm = getVM();
            vm.setPrivateKey(vm.getPrivateKey().replaceAll("[\\\\/]+","/"));
            jsonCodec.save(deployment, streamResult);
            streamResult.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the main instance of the deployment - uploads necessary files to it and installs the software.
     * @param cloudAppDeployer the cloudAppDeployer from CloudML to deploy the initial deployment.
     * @param mainInstance the main instance to configure.
     * @return a boolean if the installation of the software and the set up was successful.
     */
    private boolean setUpMainInstance(CloudAppDeployer cloudAppDeployer, VMInstance mainInstance) {
        boolean problemDuringSetup = false;
        boolean successfulInstallation = false;
        deployment.getComponentInstances().add(mainInstance);
        try {
            cloudAppDeployer.deploy(deployment);
            String firstInstanceID = mainInstance.getId();
            mainInstance.getType().setRegion(firstInstanceID.split("/")[0]); //Add region to the vm
            if (connectionSettings.getVendor().equals("openstack-nova")) {
                //update the VM and the VMSettings accordingly to the flavor spawned by CloudML
                Flavor flavor = getOpenstackInstanceFlavor(firstInstanceID);
                VM vm = mainInstance.getType();
                vm.setMinRam(flavor.getRam());
                vm.setMinStorage(flavor.getDisk());
                vm.setMinCores(flavor.getVcpus());
                vm.setProviderSpecificTypeName(flavor.getName());
                vmSettings.setMinRam(flavor.getRam());
                vmSettings.setMinStorage(flavor.getDisk());
                vmSettings.setMinCores(flavor.getVcpus());
                vmSettings.setInstanceType(flavor.getName());
            }
            uploadFiles();
            deploymentStatus.setProgress(10);
            successfulInstallation = installSoftware(firstInstanceID);
            successfulInstallation = true;
            deploymentStatus.setProgress(40);
            executeUserScript();
            deploymentStatus.setProgress(50);
        } catch (Exception e) {
            problemDuringSetup = true;
            deploymentStatus.setStatus("Problem setting up the first instance: " + e.getMessage());
            e.printStackTrace();
        }
        return !problemDuringSetup && successfulInstallation;
    }

    /**
     * Configures all the instances in the system, i.e.,
     * updates the addresses, configures condor, starts mule, etc.
     */
    public void configureInstances() {
        ArrayList<VMInstance> vmInstances = getVMInstances();
        updateAddresses(vmInstances);
        ArrayList<String> condorConfigCommands = getUpdateCondorConfigCommands(vmInstances);
        ArrayList<String> hostsFileCommands = getUpdateHostsFileCommands(vmInstances);
        ArrayList<String> muleCommands = getRunMuleCommands(vmInstances);
        for (int i = 0; i < vmInstances.size(); i++) {
            String allCommands = hostsFileCommands.get(i) + " && " + condorConfigCommands.get(i) + " && " + muleCommands.get(i) + " && sudo /etc/init.d/condor restart";
            if (i == 0) {
                allCommands += " && " + getUpdatePegasusPlanCommand(vmInstances.get(i).getId(), vmSettings.getNrOfInstances());
            }
            connector.execCommand(vmInstances.get(i).getId(), allCommands, "ubuntu", vmSettings.getPrivateKeyLocation());
        }
    }

    /**
     * Creates an image/snapshot of a virtual machine instance.
     * @param instance The instance to create the image from.
     * @param retriesCount the number of times to retry the image creation in case of an error.
     * @return The image ID of the newly created image.
     */
    private String createImage(VMInstance instance, int retriesCount) {
        if (retriesCount <= 0) {
            retriesCount = 2;
        }
        //Sync memory with the hard drive, snapshot the instance
        deploymentStatus.setStatus("Snapshotting the first instance.");
        connector.execCommand(instance.getId(), "sync", "ubuntu", vmSettings.getPrivateKeyLocation());
        String image_id = "";
        for (int i = 0; i <= retriesCount; i++) {
            if (image_id.equals("")) {
                try {
                    image_id = connector.createImage(instance);
                } catch (Exception e) {
                    deploymentStatus.setStatus("Problem snapshotting (" + e.getMessage() + "), retrying");
                }
            }
        }
        return image_id;
    }


    /**
     * Uploads the log files (jobs & transfers logs), if provided, to the instance specified
     * @param instanceID The id of a running instance
     */
    private void uploadLogs(String instanceID) {
        EstimatorSettings estimatorSettings = context.getEstimatorSettings();
        if (estimatorSettings.logsAreProvided()) {
            connector.uploadFile(estimatorSettings.getJobsLogFileLocation(),
                    "/home/ubuntu/Workflow/jobs.txt", instanceID, "ubuntu", vmSettings.getPrivateKeyLocation());
            connector.uploadFile(estimatorSettings.getTransferLogFileLocation(),
                    "/home/ubuntu/Workflow/transfers.txt", instanceID, "ubuntu", vmSettings.getPrivateKeyLocation());
        }
    }

    /**
     * Uploads all the necessary files to the first (main) instance in the deployment.
     * The files are - files from the script folder, user provided workflow script file
     * and the log files for the time estimation (if provided).
     */
    private void uploadFiles() {
        String firstInstanceID = getVMInstances().get(0).getId();
        deploymentStatus.setStatus("Uploading files to the first instance..");
        //Create Workflow folder
        connector.execCommand(firstInstanceID, "mkdir Workflow", "ubuntu", vmSettings.getPrivateKeyLocation());
        //Upload log files
        uploadLogs(firstInstanceID);
        connector.uploadFile(workflowSettings.getCustomScriptLocation(), "user_script.sh", firstInstanceID, "ubuntu", vmSettings.getPrivateKeyLocation());

        //Upload the files from the scripts folder, locate inside the jar
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(Application.class.getClassLoader());
        try {
            Arrays.asList(resolver.getResources("classpath*:scripts/**"))
                    .stream()
                    .filter(r -> r.getFilename().contains("."))
                    .forEach(r -> {
                        InputStreamPayload payload = null;
                        try {
                            payload = Payloads.newInputStreamPayload(r.getInputStream());
                            payload.getContentMetadata().setContentLength(Utilities.getStreamLength(r.getInputStream()));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (connectionSettings.getProvider().getName().equals("openstack-nova")) {
                            ((OpenStackConnector) connector).uploadFile(payload, r.getFilename(), firstInstanceID,
                                    "ubuntu", vmSettings.getPrivateKeyLocation());
                        } else {
                            ((JCloudsConnector) connector).uploadFile(payload, r.getFilename(), firstInstanceID,
                                    "ubuntu", vmSettings.getPrivateKeyLocation());
                        }
                        payload.close();
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Starts the execution of the workflow script provided by the user on the main instance.
     */
    private void executeUserScript() {
        deploymentStatus.setStatus("Executing the user script..");
        connector.execCommand(getVMInstances().get(0).getId(), "dos2unix -n user_script.sh user_script.sh && bash user_script.sh", "ubuntu", vmSettings.getPrivateKeyLocation());
    }

    /**
     * Runs the install_software.sh script on the first instance.
     * The script will setup Pegasus, Condor, Mule, METIS, etc.
     *
     * @param firstInstanceID The id of the first instance
     * @return a boolean if the software was installed successfully.
     */
    private boolean installSoftware(String firstInstanceID) {
        deploymentStatus.setStatus("Installing software.");
        connector.execCommand(firstInstanceID, "sudo bash install_software.sh > out.txt", "ubuntu", vmSettings.getPrivateKeyLocation());
        updateAddresses(getVMInstances());
        String command = "condor_version ; pegasus-version ; python /home/ubuntu/Mule/bin/mule ; cat /usr/bin/pegasus-transfer | grep \"python /home/ubuntu/Mule\"";
        String output = "";
        if (connectionSettings.getVendor().equals("openstack-nova")) {
            output = ((OpenStackConnector) connector).execCommandWithResponse(firstInstanceID, command,
                    "ubuntu", vmSettings.getPrivateKeyLocation());
        } else {
            output = ((JCloudsConnector) connector).execCommandWithResponse(firstInstanceID, command,
                    "ubuntu", vmSettings.getPrivateKeyLocation());
        }
        boolean successInstallation = output.contains("4.6.2") &&
                output.contains("$CondorPlatform: x86_64") && output.contains("rls_clear") &&
                output.contains("python /home/ubuntu/Mule/bin/mule");
        return successInstallation;
    }

    /**
     * Get commands that add new lines to the running instances hosts file, in the format of:
     * <p>PRIVATE_IP_OF_MACHINE_0 MACHINE_0 name-0</p>
     * <p>...</p>
     * <p>PRIVATE_IP_OF_MACHINE_n MACHINE_n name-n</p>
     * The modified hosts file is needed for the communication between the machines for Condor.
     * If the provider is aws-ec2, the name, for example, is in the format of ip-172-31-11-253.
     *
     * @param vmInstances the running instances in the current model.
     * @return returns an array of the commands to be run on each instance, i.e, arraylist.get(0) returns the commands to be run on the first instance
     */
    private ArrayList<String> getUpdateHostsFileCommands(ArrayList<VMInstance> vmInstances) {
        ArrayList<String> commandsToRun = new ArrayList<>();
        String clearHosts = "sudo sed -i '/.*/d' /etc/hosts";
        String changeHostsCommand = clearHosts + "&&" + "echo -e '";
        for (int i = 0; i < vmInstances.size(); i++) {
            String privateAddress = vmInstances.get(i).getPrivateAddress();
            String hostLine;
            if (connectionSettings.getVendor().equals("aws-ec2")) {
                hostLine = privateAddress + " MACHINE_" + i + " ip-" + privateAddress.replaceAll("\\.", "-");
            } else {
                hostLine = privateAddress + " MACHINE_" + i + " " + connectionSettings.getCredentials().getLogin() + "-" +
                        +startTime + "-" + i;
            }
            changeHostsCommand += hostLine + "\\n";
        }
        changeHostsCommand += "' | sudo tee /etc/hosts";
        for (int i = 0; i < vmInstances.size(); i++) {
            commandsToRun.add(i, changeHostsCommand);
        }
        return commandsToRun;
    }

    /**
     * Returns commands for each instance, that updates the condor config in /etc/condor/condor_config.local. <br>
     * Changes the MAIN_IP to the first instances IP, <br>
     * the DAEMON_LIST to MASTER, STARTD and <br>
     * the PRIVATE_IP to the private IP of the current instance.
     *
     * @param vmInstances The virtual machines to run the command on.
     * @return returns an array of the commands to be run on each instance, i.e, arraylist.get(0) returns the commands to be run on the first instance
     */
    private ArrayList<String> getUpdateCondorConfigCommands(ArrayList<VMInstance> vmInstances) {
        ArrayList<String> commandsToRun = new ArrayList<>();
        String condorFile = "/etc/condor/condor_config.local";
        String mainNodeIP = vmInstances.get(0).getPrivateAddress();
        String changeMainIPCommand = "sudo sed -i 's/MAIN_IP = $(FULL_HOSTNAME)/MAIN_IP = " + mainNodeIP + "/g' " + condorFile;
        String changeDaemonList = "sudo sed -i 's/DAEMON_LIST = COLLECTOR, MASTER, NEGOTIATOR, SCHEDD, STARTD/" +
                "DAEMON_LIST = MASTER, STARTD/g' " + condorFile;
        for (int i = 0; i < vmInstances.size(); i++) {
            String completeCommand = "";
            String changePrivateIP = "sudo sed -i 's/PRIVATE_IP = *.*.*.*/PRIVATE_IP = " + vmInstances.get(i).getPrivateAddress() + "/g' " + condorFile;
            completeCommand += changePrivateIP;
            if (i != 0) {
                completeCommand += "&& " + changeMainIPCommand + " && " + changeDaemonList;
            }
            commandsToRun.add(i, completeCommand);
        }
        return commandsToRun;
    }

    /**
     * Get the commands that starts the mule scripts on the instances.
     * This means, that on the first instance
     * the mule-rls server and mule-cache is started, and on the rest of the instances, only the cache servers are started.
     *
     * @param vmInstances The virtual machines to run the command on.
     * @return returns an array of the commands to be run on each instance, i.e, arraylist.get(0) returns the commands to be run on the first instance
     */
    private ArrayList<String> getRunMuleCommands(ArrayList<VMInstance> vmInstances) {
        ArrayList<String> commandsToRun = new ArrayList<>();
        String mainNodeIP = vmInstances.get(0).getPrivateAddress();
        for (int i = 0; i < vmInstances.size(); i++) {
            String id = vmInstances.get(i).getId();
            String command = "";
            if (i == 0) {
                command = "sudo bash run_mule_mainnode.sh " + mainNodeIP;
            } else {
                command = "sudo bash run_mule_workernode.sh " + mainNodeIP;
            }
            commandsToRun.add(i, command);
        }
        return commandsToRun;
    }

    /**
     * Returns a command that updates the plan.sh file, which consists of the machines to run the workflow on. <br>
     * If <i>n</i> is the nrOfInstances, lines <i>MACHINE_0,MACHINE_1,....,MACHINE_n</i> <br>
     * are added to the plan.sh file in the folder /home/ubuntu/Workflow after the --sites keyword.
     *
     * @param nrOfInstances The number of instances in the current deployment.
     * @return returns the update pegasus plan command.
     */
    private String getUpdatePegasusPlanCommand(String firstInstanceId, int nrOfInstances) {
        String machines = "";
        for (int i = 0; i < nrOfInstances; i++) {
            machines += "MACHINE_" + i + ",";
        }
        machines = machines.replaceFirst(",$", ""); //Remove the last comma
        String updatePlanFile = "sed -i 's/MACHINE.* --out/" + machines + " --out/g' /home/ubuntu/Workflow/plan.sh";
        return updatePlanFile;
    }

    /**
     * Updates the private and public addresses of the instances. <br>
     * This has to be done, because Openstack can have a public ip, that is reported as private ip by jclouds. <br>
     * For example (172.x.x.x) is considered a private IP by jclouds, although it could be a public one.
     *
     * @param vmInstances The virtual machines to iterate over and to update.
     */
    private void updateAddresses(ArrayList<VMInstance> vmInstances) {
        for (VMInstance vmInstance : vmInstances) {
            String id = vmInstance.getId();
            String publicAddr = "", privateAddr = "";
            if (connectionSettings.getVendor().equals("openstack-nova")) {
                ArrayList<String> privateAddresses = Lists.newArrayList(((OpenStackConnector) connector).getVMById(id).getPrivateAddresses());
                if (privateAddresses.size() >= 2) {
                    String adr0 = privateAddresses.get(0), adr1 = privateAddresses.get(1);
                    boolean includesPattern = adr0.contains(vmSettings.getPublicAddressPattern());
                    publicAddr = (includesPattern) ? adr0 : adr1;
                    privateAddr = (includesPattern) ? adr1 : adr0;
                } else {
                    publicAddr = ((OpenStackConnector) connector).getVMById(id).getPublicAddresses().iterator().next();
                    privateAddr = (privateAddresses.get(0));
                }
            } else { // "amazon-ec2"
                publicAddr = ((JCloudsConnector) connector).getVMById(id).getPublicAddresses().iterator().next();
                privateAddr = ((JCloudsConnector) connector).getVMById(id).getPrivateAddresses().iterator().next();
            }
            vmInstance.setPublicAddress(publicAddr);
            vmInstance.setPrivateAddress(privateAddr);
            System.out.println("Did update the addresses for instance " + vmInstance.getName() + " to :" + vmInstance.getPublicAddress() + " " + vmInstance.getPrivateAddress());
        }
    }


    /**
     * Restarts the current deployment, i.e., terminates it and starts it again.
     */
    public void restartDeployment() {
        try {
            terminateDeployment();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.deployment = new Deployment();
        startTime = System.currentTimeMillis();
        deploy();
    }

    /**
     * Terminates the current deployment, i.e., kills the instances in the deployment.
     */
    public void terminateDeployment() throws InterruptedException { //TODO: If no instances are running, wait them to have the state 'running' before killing or continue with restarting ?
        deploymentStatus.setStatusAndProgress("Terminating instances..", 0);
        for (VMInstance vmInstance : getVMInstances()) {
            try {
                if (!vmInstance.getId().equals("") && vmInstance.getId() != null) {
                    System.out.println("Trying to destroy vm by the id " + vmInstance.getId());
                    connector.destroyVM(vmInstance.getId());
                }
            } catch (Exception e) {
                e.printStackTrace(); //TODO: Show a message in the panel, but continue with restarting the deplyoment?
            }
        }
        deployment.getComponentInstances().clear();
        deploymentStatus.setStatus("None");
    }

    /**
     * Gets the openstack flavors in the current project from the region 'RegionOne'
     * @return the ArrayList of different flavors, i.e., types of instances available to the current user.
     */
    public ArrayList<Flavor> getOpenStackFlavors() {
        ContextBuilder builder = generateOpenstackBuilder();
        NovaApi serverApi = builder.buildApi(NovaApi.class);
        ArrayList<Flavor> openstackFlavors = new ArrayList<Flavor>();
        serverApi.getFlavorApi("RegionOne").listInDetail().forEach(flavors ->
                flavors.forEach(flavor -> openstackFlavors.add(flavor))
        );
        return openstackFlavors;
    }

    /**
     * Generate the openstack context builder, which can be used to build different API access points.
     * @return ContextBuilder
     */
    private ContextBuilder generateOpenstackBuilder() {
        if (!connectionSettings.getVendor().equals("openstack-nova")) {
            throw new IllegalStateException("Can not get the open-stack flavors for amazon-ec2!");
        }
        Iterable<Module> modules = ImmutableSet.<Module>of(new NullLoggingModule());
        Properties overrides = new Properties();
        overrides.setProperty(PROPERTY_CONNECTION_TIMEOUT, 0 + "");
        overrides.setProperty(PROPERTY_SO_TIMEOUT, 0 + "");
        Provider provider = connectionSettings.getProvider();
        ContextBuilder builder = ContextBuilder.newBuilder("openstack-nova")
                .endpoint(connectionSettings.getOpenstackEndpoint())
                .credentials(provider.getCredentials().getLogin(), provider.getCredentials().getPassword())
                .modules(modules)
                .overrides(overrides);
        return builder;
    }

    /**
     *
     * @param instanceId The id of the instance (with region)
     * @return the name of flavor of the instance.
     */
    private Flavor getOpenstackInstanceFlavor(String instanceId) {
        String region = "RegionOne";
        ContextBuilder builder = generateOpenstackBuilder();
        NovaApi novaApi = builder.buildApi(NovaApi.class);
        ServerApi serverApi = novaApi.getServerApi(region);
        FlavorApi flavorApi = novaApi.getFlavorApi(region);
        return flavorApi.get(serverApi.get(instanceId.split("/")[1]).getFlavor().getId());
    }

    /**
     * Returns the component instances of the current deployment run.
     * @return the ArrayList of instances currently in the deployment.
     */
    public ArrayList<VMInstance> getVMInstances() {
        return new ArrayList<>(deployment.getComponentInstances().onlyVMs());
    }

    /**
     * Returns the virtual machine associated with the current homogeneous deployment.
     * @return VM of the current deployment
     */
    public VM getVM() {
        return deployment.getComponents().onlyVMs().iterator().next();
    }
}