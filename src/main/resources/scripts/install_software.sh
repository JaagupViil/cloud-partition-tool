#!/bin/bash
#------------------------------------------------------------#
# This script is going to install all the necessary software #
# to execute the workflows - Pegasus, Condor & Mule          #
#------------------------------------------------------------#

echo "#--------------------------------------------------------#"
echo "#" Downloading necessary packages
echo "#--------------------------------------------------------#"
sudo add-apt-repository -y ppa:saiarcot895/myppa
sudo apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install apt-fast
sudo apt-fast -qq -y -f install gcc make libdb5.1-dev libpq5 libmysqlclient18 openjdk-7-jre openjdk-7-jre-headless openjdk-6-jre openjdk-6-jre-headless dos2unix python-dev metis-edf metis git
NB_CORES=$(grep -c '^processor' /proc/cpuinfo)
echo "#--------------------------------------------------------#"
echo "#" Installing mule
echo "#--------------------------------------------------------#"
git clone https://gitlab.com/juve/mule.git Mule
cd /home/ubuntu/Mule
sed -i 's@all: lib/bsddb3 src/libbloom.so@all: lib/bsddb3@g' Makefile
sed -i 's@python setup.py build@python setup.py --berkeley-db=/home/ubuntu/Mule/lib/bsddb3 build@g' Makefile
find -type f -name 'mule*' -exec sed -i '1c#!/usr/bin/env python2' {} \;
sudo make -j$((NB_CORES+1)) -l${NB_CORES} &
mkdir temp var
sudo chown nobody:nogroup temp
sudo chown ubuntu:ubuntu var
cd ..
echo "Mule installation finished!"
echo "#-------------------------------------------------------#"
echo "#" Installing Pegasus
echo "#-------------------------------------------------------#"
wget http://download.pegasus.isi.edu/pegasus/4.6.2/pegasus_4.6.2-1+ubuntu14_amd64.deb --timeout=10
sudo dpkg -i pegasus_4.6.2-1+ubuntu14_amd64.deb
#Add the MuleHandler to pegasus-transfer
sed -i '/self._available_handlers.append( SymlinkHandler() )/a\        \self._available_handlers.append( MuleHandler() )' /usr/bin/pegasus-transfer
read -d '' mulehandler << "EOF"
class MuleHandler(TransferHandlerBase):
    """
    Handler for Mule
    """
    _name = "MuleHandler"
    _mkdir_cleanup_protocols = ["mule"]
    _protocol_map = ["file->mule","mule->file"]

    def do_mkdirs(self, transfers):
        successful_l = []
        failed_l = []
        for t in transfers:
            cmd = "sudo /bin/mkdir -p '%s' " % (t.get_path())
            try:
                tc = TimedCommand(cmd)
                tc.run()
            except RuntimeError, err:
                logger.error(err)
                failed_l.append(t)
                continue
            successful_l.append(t)
        return [successful_l, failed_l]

    def do_transfers(self, transfer_list):
        logger.info("Using mule handler")
        successful_l = []
        failed_l = []
        for t in transfer_list:
            t_start = time.time()
            split_url = t.src_url().split("/")
            filename = split_url[len(split_url)-1]
            cmd = "python /home/ubuntu/Mule/bin/mule"
            if t.get_dst_proto() == "file":
                prepare_local_dir(os.path.dirname(t.get_dst_path()))
                if "/var/lib/condor" in t.get_dst_path():
                    cmd += " get "+filename+" "+t.get_dst_path()
                else:
                    cmd += " get "+filename+" /home/ubuntu/Mule/temp/"+filename
            else:
                split_url = t.dst_url().split("/")
                filename = split_url[len(split_url)-1]
                cmd += " put "+t.get_src_path()+" "+filename
            try:
                TimedCommand(cmd).run()
                if t.get_dst_proto() == "file" and "/var/lib/condor/" not in t.get_dst_path():
                    change_rights = "sudo /bin/chown ubuntu:ubuntu /home/ubuntu/Mule/temp/"+filename
                    move = "sudo mv /home/ubuntu/Mule/temp/"+filename+" "+t.get_dst_path()
                    TimedCommand(change_rights).run()
                    TimedCommand(move).run()
                    stats_add(t.get_dst_path())
                else:
                    stats_add(t.get_src_path())
            except RuntimeError, err:
                logger.error(err)
                self._post_transfer_attempt(t, False, t_start)
                failed_l.append(t)
                continue
            self._post_transfer_attempt(t, True, t_start)
            successful_l.append(t)
        return [successful_l, failed_l]

    def do_removes(self, transfers):
        successful_l = []
        failed_l = []
        for t in transfers:
            cmd = "/bin/rm -f"
            if t.get_recursive():
                cmd += " -r "
                cmd += " '%s' " % (t.get_path())
            try:
                tc = TimedCommand(cmd)
                tc.run()
            except RuntimeError, err:
                logger.error(err)
                failed_l.append(t)
                continue
            successful_l.append(t)
        return [successful_l, failed_l]


EOF
awk -v var="$mulehandler" '/class HTTPHandler/{print var}1' /usr/bin/pegasus-transfer > /usr/bin/pegasus-transfer-mule
cp /usr/bin/pegasus-transfer-mule /usr/bin/pegasus-transfer
echo "Pegasus installation finished!"

echo "#--------------------------------------------------------#"
echo "#" Installing Condor
echo "#--------------------------------------------------------#"
sudo DEBIAN_FRONTEND=noninteractive apt-fast -y install htcondor
IP=$(/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
read -d '' config << "EOF"
MAIN_IP = $(FULL_HOSTNAME)
PRIVATE_IP = 0.0.0.0
CONDOR_HOST = $(MAIN_IP)
NEGOTIATOR_HOST = $(MAIN_IP)
COLLECTOR_NAME = $(MAIN_IP)
START = TRUE
SUSPEND = FALSE
PREEMPT = FALSE
KILL = FALSE
DAEMON_LIST = COLLECTOR, MASTER, NEGOTIATOR, SCHEDD, STARTD
ALLOW_WRITE = *
ALLOW_READ = *
MAX_JOBS_RUNNING = 500
HIGHPORT =  16000
LOWPORT = 9600
#---------
NETWORK_INTERFACE = $(PRIVATE_IP)
SEC_DEFAULT_AUTHENTICATION = NEVER
SEC_DEFAULT_NEGOTIATION = NEVER
#---------
#---------
UPDATE_INTERVAL = $RANDOM_INTEGER(230, 370)
MASTER_UPDATE_INTERVAL = $RANDOM_INTEGER(230, 370)
# Number of jobs DAGMan will submit at once (default 5).
DAGMAN_MAX_SUBMITS_PER_INTERVAL=150
# Number of seconds between checks for submitting more jobs (default 5)
DAGMAN_USER_LOG_SCAN_INTERVAL=1
# Amount of time slot will do work for a user/schedd before a new negotiation cycle is needed.
# Within this time, the schedd will instantly run another job on the slot (if there are jobs in queue!)
# Default 1200
CLAIM_WORKLIFE=1800
# How much time the schedd will hold onto the slot even if it has no jobs to run.
# This gives DAGMan some time to submit more if it runs out of jobs.
# Default 20
DAGMAN_HOLD_CLAIM_TIME=60

CONTINUE = True
WANT_VACATE = False
WANT_SUSPEND = True
SUSPEND_VANILLA = False
WANT_SUSPEND_VANILLA = True
STARTD_EXPRS = START
KILL_VANILLA = False

SYSAPI_GET_LOADAVG = False
EOF
echo "$config" >> /etc/condor/condor_config.local
echo "Condor installation and configuration done!"

echo "#--------------------------------------------------------#"
echo "#" Workflow folder config
echo "#--------------------------------------------------------#"

sudo chown ubuntu:ubuntu Workflow
cd /home/ubuntu/Workflow
read -d '' plan << "EOF"
#!/bin/sh
echo "Planning the workflow..."
sudo rm -rf /home/ubuntu/Mule/temp/*
pegasus-plan --conf inputs/pegasus.properties --sites MACHINES --output-site local --staging-site MACHINE_0 --dir submit --dax inputs/dax.xml --nocleanup --submit
EOF
echo "$plan" >> /home/ubuntu/Workflow/plan.sh
echo "Workflow folder config done!"

echo "#--------------------------------------------------------#"
echo "#" Adding additional rights for commands used in mule to /etc/sudoers
echo "#--------------------------------------------------------#"
read -r -d '' sudoers << EOM
ubuntu ALL = NOPASSWD: /bin/chown, /bin/mv
nobody ALL = NOPASSWD: /bin/chown, /bin/mv
root ALL = NOPASSWD: /bin/chown, /bin/mv
EOM
sudo sh -c "echo \"$sudoers\" >> /etc/sudoers"
echo "All set up!"