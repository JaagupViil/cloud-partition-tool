#!bin/bash
dax=/home/ubuntu/Workflow/inputs/dax.xml
transfers=/home/ubuntu/Workflow/transfers.txt
jobs=/home/ubuntu/Workflow/jobs.txt
summary=/home/ubuntu/Workflow/summary.txt

MACHINES_COUNT=$(grep MACHINE /etc/hosts |wc -l)

mv dax_to_metis.py /home/ubuntu/Workflow/dax_to_metis.py
mv create_partitioned_dax.py /home/ubuntu/Workflow/create_partitioned_dax.py
cd Workflow

CORES="$(nproc)"
TOTAL_CORES=$(($CORES*MACHINES_COUNT))
LAST_EXECUTION_FOLDER="$(ls -1dt /home/ubuntu/Workflow/submit/ubuntu/pegasus/*/* | head -n 1)"

#Failsafe function to generate the logs if pegasus-statistics won't work and append the execution information at the end of jobs.txt
#Sometimes, pegasus-statistics encounters some issues, so we have to use pegasusus-monitord to 'replay' the execution
#before running the statistics again.
#Function takes a boolean argument, if true generates only summary, else both summary and job_stats from the execution.
function generate_logs {
        if [[ $1 == true ]]; then
                 pegasus-statistics -s summary -o /home/ubuntu/Workflow/
        else
                 pegasus-statistics -s jb_stats,summary -o /home/ubuntu/Workflow/
        fi
        if [[ -e "$summary" && -e "$jobs" ]]; then
                echo "Generated logs"
        else
                pegasus-monitord *.*.dagman.out -r
                if [[ $1 == true ]]; then
                        pegasus-statistics -s summary -o /home/ubuntu/Workflow/
                else
                        pegasus-statistics -s jb_stats,summary -o /home/ubuntu/Workflow/
                fi
                echo "Generated logs"
        fi
        #Append the run information to the end of the jobs.txt
        cd /home/ubuntu/Workflow
        RUNTIME="$(grep -oP '(?<=Workflow wall time ).*' summary.txt)"
        RUNTIME_NOSPACE="$(echo -e "${RUNTIME}" | tr -d '[:space:]')"
        echo "Total cores and execution time for "$LAST_EXECUTION_FOLDER":"$TOTAL_CORES$RUNTIME_NOSPACE >> jobs.txt
}

if [[ -e "$dax" && -e "$transfers" && -e "$jobs" ]]; then
    echo "Logs provided by the user, starting the partitioning!"
    MYVAR="$(grep "Total cores and execution time" jobs.txt | cut -d ':' -f 1 | tail -n 1)"
    LAST_EXECUTION_FOLDER_IN_JOBS=${MYVAR##*for }
    LAST_EXECUTION_CORES_IN_JOBS="$(grep "Total cores and execution time" jobs.txt | cut -d ':' -f 2 | tail -n 1)"
    #Generate the summary log if the new execution folder is different from the last, or if the nr of cores in this execution differs from the last and
    #add the new execution information to the end of the jobs.txt
    echo $LAST_EXECUTION_FOLDER
    if [[ -e $LAST_EXECUTION_FOLDER ]]; then
        if [[ $LAST_EXECUTION_FOLDER_IN_JOBS != $LAST_EXECUTION_FOLDER || $TOTAL_CORES != $LAST_EXECUTION_CORES_IN_JOBS ]]; then
            cd $LAST_EXECUTION_FOLDER
            generate_logs true
        fi
    fi
else
    cd $LAST_EXECUTION_FOLDER
    #Generate the transfers.txt, by going over the log files of each job
    grep -A 5000 "staging out output files" *.err.* | grep Stats > /home/ubuntu/Workflow/transfers.txt
    generate_logs false
fi
cd /home/ubuntu/Workflow
python dax_to_metis.py /home/ubuntu/Workflow/inputs/dax.xml transfers.txt

# RUN IT WITH GPMETIS (if fails use pmetis)
rm metis_dax.part.*
gpmetis metis_dax $MACHINES_COUNT -objtype=vol -ncuts=20
metis_file=/home/ubuntu/Workflow/metis_dax.part.$MACHINES_COUNT
if [[ ! -e "$metis_file" ]]; then
    pmetis metis_dax $MACHINES_COUNT
fi
# Parse metis output and write it to new dag
python create_partitioned_dax.py metis_dax.part.* /home/ubuntu/Workflow/inputs/dax.xml
#move partitioned_dax to inputs
mv partitioned_dax.xml inputs/partitioned_dax.xml
# copy plan.sh to partitioning_plan and change the dax location
cp plan.sh partitioning_plan.sh
sed -i 's@inputs/dax.xml@inputs/partitioned_dax.xml@g' partitioning_plan.sh

partitioned_dax=/home/ubuntu/Workflow/inputs/partitioned_dax.xml
if [[ -e "$dax" && -e "$transfers" && -e "$jobs" && -e "$partitioned_dax" ]]; then
    echo "Partitioning successful!"
else
    echo "Partitioning unsuccessful!"
fi
