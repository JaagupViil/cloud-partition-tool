import re,collections,sys,csv

#############################
#############################
# This python script takes a DAX workflow file, and a transfers.txt file (contains the sent data of each job on each line in the format of:
# fibonacci_ID0000001.err.000-2017-01-16 16:44:37,496    INFO:  Stats: Total 2 transfers, 2.3 MB transferred in 2 seconds. Rate: 1.0 MB/s (8.1 Mb/s) )
# and generates a METIS graph file (http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/manual.pdf) that will be used later to partition
# the initial DAX between the machines available in the cluster.
# The data gotten from the transfers.txt will be normalized (linear transformation) in order to avoid integer overflows with METIS.
# For example, if the smallest sent data is 10 "units" and the largest is 1000, the respective numbers are scaled down to 1 and 100, while
# preserving the ratio of the data transfers.
# The formula used to scale value x to a new range [a,b] , where a=1, and b=(max transfer size)/(min transfer size), is (b-a) * ( (x - min[x]) / (max[x] - min[x]) ) + a
#############################
#############################

if len(sys.argv) != 3:
    print "Not enough arguments"
    sys.exit()

dax_filename=sys.argv[1]
transfers_filename=sys.argv[2]

# Iterate over the dax
# and get the id's and name's of each job
# return a dictionary with id's and the corresponding job names, with the index, e.g
# ID000001 = JOBNAME33:1
def getJobIDsAndNames(dax_filename):
    dax = open(dax_filename)
    ids = collections.OrderedDict()
    index = 1
    for line in dax:
        line = line.strip("\n")
        if ("id=" in line and "name=" in line):
            id = line.split("id=")[1].split(" ")[0].replace("\"","")
            name = line.split("name=")[1].split(" ")[0].replace("\"","").strip(">")
            ids[id] = name + ":" + str(index)
            index += 1
    return ids

# Create an initial undirected graph in the form of a dictionary
# from the dax file in the format of:
# JOBNAME_ID4 = {JOBNAME_ID2 : 'to', JOBNAME_ID3 : 'from'}
# The "to" and "from" represents whether the job is a child node (to) or a parent node (from), e.g,
# JOBNAME_ID4 comes from JOBNAME_ID3 and the connection goes to JOBNAME_ID2
def buildConnections(dax_filename):
    names = getJobIDsAndNames(dax_filename)
    dax = open(dax_filename)
    connections = {}
    parent_id_name = ""
    for line in dax:
        line = line.strip("\n")
        if ("child ref" in line):
            parent_id = line.split("\"")[1]
            parent_id_name = names[parent_id].split(":")[0]+"_"+parent_id
            if parent_id_name not in connections:
                connections[parent_id_name] = {}
        if ("parent ref" in line):
            child_id = line.split("\"")[1]
            child_id_name = names[child_id].split(":")[0]+"_"+child_id
            if (child_id_name not in connections):
                connections[child_id_name] = {}
            connections[child_id_name][parent_id_name] = "to"
            connections[parent_id_name][child_id_name] = "from"
            
    return connections


# Replaces the "to"'s and "from"'s in the dictionary
# with the real transfer sizes in bytes gotten from parsing pegasus logs.
# E.g, JOBNAME_ID4 = {JOBNAME_ID2 : 'to', JOBNAME_ID3 : 'from'}, transforms to
# JOBNAME_ID4 = {JOBNAME_ID2 : '400993', JOBNAME_ID3 : '20300'}
# min_transfer size and max_transfer size are used to normalize the data
# because large numbers could cause integer overflow later on when using METIS

min_transfer = float('inf') ## Used for the normalization in the writeToFile function
max_transfer = 0            ## Used for the normalization in the writeToFile function
def addTransferSizes(connections,transfers_filename):
    global min_transfer, max_transfer
    transfers_file = open(transfers_filename)
    for line in transfers_file:
        line = line.strip("\n")
        job_name = line.split(":")[0].split(".")[0]
        transfer_size = line.split("transfers,")[1].split(" ")
        size_in_bytes = get_bytes(transfer_size[1],transfer_size[2])
        if (float(size_in_bytes) < min_transfer):
            min_transfer = float(size_in_bytes)
        if (float(size_in_bytes) > max_transfer):
            max_transfer = float(size_in_bytes)
        for item in connections[job_name]:
            if(connections[job_name][item] == "to"):
                connections[job_name][item] = size_in_bytes
    for item in connections:
        for job_name in connections[item]:
            if (connections[item][job_name] == "from"):
                connections[item][job_name] = connections[job_name][item]

def get_bytes(size, suffix):
    size = float(size)
    if 'KB' in suffix:
        size = size * 1024
    elif 'MB' in suffix:
        size =  size * 1024 * 1024
    elif 'GB' in suffix:
        size =  size * 1024 * 1024 * 1024

    return int(size)

# Takes the jobs.txt file outputted from pegasus-statistics, connections dictionary
# and outputs a dictionary with each job's and a metric (CPU or execution TIME)
# E.g, jobs_with_metrics = {JOBNAME_ID4 : 42s}
def getJobMetrics(jobs_filename,connections,metricType):
    reader = csv.reader(open(jobs_filename),delimiter='\t')
    jobs_with_metrics = {}
    for line in reader:
        if (line != []):
            line = (line[0].split())
        if (len(line)>=15):
            job_name = line[0]
            if metricType == 'CPU':
                metric_value = line[6]
            elif metricType == 'Time':
                metric_value = line[10]
            if (job_name in connections):
                jobs_with_metrics[job_name] = metric_value
    return jobs_with_metrics

# Write the connections of the DAX with the transfer sizes (weights)
# to a graph file named 'metis_dax', which will be used later on
# with the METIS library.
def writeToFile(connections,dax_filename):
    connection_size = 0
    for i in connections:
        connection_size += len(connections[i])
    sys.stdout=open('metis_dax','w')
    ids = getJobIDsAndNames(dax_filename)
    print len(connections),connection_size/2,1 #print the first line of the METIS graph file
    for id in ids:
        job_name = ids[id].split(":")[0]+"_"+id
        connected_nodes = connections[job_name] #get the node connections of a job
        for node in connected_nodes:
            node_id = "ID"+node.split("ID")[1]
            transfer_size = connected_nodes[node]
            normalized_size = ((((max_transfer/min_transfer)-1)*(transfer_size-min_transfer))/(max_transfer-min_transfer))+1
            node_index = ids[node_id].split(":")[1]
            print node_index,str(int(normalized_size)),
        print
    sys.stdout.close()


connections = (buildConnections(dax_filename))
addTransferSizes(connections,transfers_filename)
writeToFile(connections,dax_filename)