#!/bin/bash
#--------------------------------------#
#This script will start the cache server
#on this (worker) node. The mule cache
#is going to be run with user nobody
#and group nogroup because otherwise
#it wont work (condor runs jobs with 
#these groups)
#--------------------------------------#

cd /home/ubuntu
RLS_IP=$1
MULE_PATH=$PWD'/Mule/bin/'
cd Mule
sudo rm -rf var
sudo mkdir var
sudo chown nobody:nogroup var
cd /tmp
sudo rm -rf mule
sudo mkdir mule
sudo chown nobody:nogroup mule
sudo -u nobody -g nogroup python $MULE_PATH'mule-cache' --rls $RLS_IP