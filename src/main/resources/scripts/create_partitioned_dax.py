import sys

#############################
#############################
# This python script takes a DAX workflow and a METIS partition file, and maps the METIS results
# to a new DAX "partitioned_dax.xml", adding <profile namespace="hints" key="execution.site">MACHINE_N</profile>
# after each job, where MACHINE_N is the corresponding partition for a job, gotten from METIS.
#############################
#############################

if len(sys.argv) != 3:
    print "Not enough arguments"
    sys.exit()

metis_filename = str(sys.argv[1])
dax_filename = str(sys.argv[2])

metis_file = open(metis_filename)
dax_file = open(dax_filename)

metis_partitions = []
for line in metis_file:
    metis_partitions.append(int(line.strip('\n\r')))

dax = []
for line in dax_file:
    line = line.strip("\n")
    dax.append(line)

index = 0
sys.stdout=open('partitioned_dax.xml','w')

for i,line in enumerate(dax):
    print line
    if (i+1 < len(dax) and "</job>" in dax[i+1]):
        machine_nr = str(metis_partitions[index])
        print ("    <profile namespace=\"hints\" key=\"execution.site\">MACHINE_"+machine_nr+"</profile>")
        index += 1

sys.stdout.close()
