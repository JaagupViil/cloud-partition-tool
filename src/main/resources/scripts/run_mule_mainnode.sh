#!/bin/bash
#-------------------------------------#
#This script starts the rls server
#on the mainnode and the cache
#specified with the rls server ip     
#-------------------------------------#

cd /home/ubuntu
RLS_IP=$1
MULE_PATH=$PWD'/Mule/bin/'
cd Mule
sudo rm -rf var
sudo mkdir var
sudo chown nobody:nogroup var
cd /tmp
sudo rm -rf mule
sudo mkdir mule
sudo chown nobody:nogroup mule
python $MULE_PATH'mule-rls'
sudo -u nobody -g nogroup python $MULE_PATH'mule-cache' --rls $RLS_IP
