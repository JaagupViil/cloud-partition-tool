var canGetWorkflowStatus = false;
var deploymentStatus = "";
var chart;

$(document).ready(function() {
    getDeploymentStatus();
});

function getDeploymentStatus() {
    $.ajax({
        url: 'status/deploymentStatus',
        success: function(data) {
            deploymentStatus = data.status;
            $("#deployment_status").text(data.status);
            $("#deployment_progress_bar").css('width',data.progress+'%').attr('aria-valuenow',data.progress);
            $("#deploy_status_percent").text(data.progress+'%');
            if (data.status == "Deployment finished!" && canGetWorkflowStatus == false) {
                canGetWorkflowStatus = true;
                getWorkflowStatus();
            }
        }
    });
    if (deploymentStatus != "Deployment finished!") {
        setTimeout(getDeploymentStatus, 25000);
    }

}

var canPartitionTheWorkflow = true;
function getWorkflowStatus() {
    if (canGetWorkflowStatus) {
        if ($("#workflow_status").html() === "None") {
            $("#workflow_status").html("Fetching..");
        }
        $.ajax({
            url: 'status/workflowStatus',
            success: function(data) {
                $("#workflow_status").html('<pre>'+data+'</pre>');
                if (data.includes("Success") && canPartitionTheWorkflow) {
                    $("#partitioning_status").html("Partitioning the workflow..");
                    $.get('/workflow/partition',function(data) {
                        if (data === true) {
                            $("#partitioning_status").html("Workflow partitioned!");
                            $("#download_links").append("<b>Log files:</b><br/>")
                            $("#download_links").append("<a href='/download/transfersLog'>Download transfers log file.</a><br/>")
                            $("#download_links").append("<a href='/download/jobsLog''>Download jobs log file.</a><br/>")
                            $("#download_links").append("<a href='/download/DAX'>Download the workflow's dax.</a><br/>")
                            $("#download_links").append("<a href='#' onclick='startExecution()'>Click here to execute the partitioned workflow!</a><br/>")
                            global.getTimeEstimation(false);
                        } else {
                            $("#partitioning_status").html("Problem partitioning the workflow!");
                        }
                    });
                    canPartitionTheWorkflow = false;
                }
                if (data.includes("Success") == false) {
                    $("#download_links").empty();
                    $("#partitioning_status").empty();
                }
            }
        });
    }
    setTimeout(getWorkflowStatus, 25000);
}

function terminateDeployment() {
    if (deploymentStatus === "None") {
        swal("Error","Can't terminate deployment!",'error');
    } else {
        clear();
        $("#deployment_status").text("Terminating!");
        $.post('/deployment/terminate',function(data) {});
    }
}
function restartDeployment() {
    if (deploymentStatus == "None") {
        swal("Error","Can't restart the deployment yet!",'error');
    } else {
        clear();
        $("#deployment_status").text("Restarting!");
        $.post('/deployment/restart',function(data) {});
    }
}
function terminateExecution() {
    if (canGetWorkflowStatus) {
        canGetWorkflowStatus = false;
        clear();
        $("#workflow_status").html("Terminating!");
        $.get('/workflow/terminate',function(data) {
            $("#workflow_status").html("Terminated!");
        });
    } else {
        swal("Error","Can't terminate execution yet!",'error');
    }
}
function restartExecution() {
    if (canGetWorkflowStatus) {
        canGetWorkflowStatus = false;
        clear();
        $("#workflow_status").html("Restarting!");
        $.get('/workflow/restart',function(data) {
            location.reload();
        });
    } else {
        swal("Error","Can't restart execution yet!",'error');
    }
}

function startExecution() {
    if (canGetWorkflowStatus) {
        canGetWorkflowStatus = false;
        clear();
        $("#workflow_status").text("Starting the execution..");
        $.get('/workflow/start',function(data) {
            location.reload();
        });
    } else {
//        alert ("Can't start execution yet!");
        swal("Error","Can't start execution yet!",'error');
    }
}

function clear() {
    $("#workflow_status").empty();
    $("#download_links").empty();
    $("#partitioning_status").empty();
    $("#canvas").hide();
    global.clearEstimation();
}