
function validateSettingsForm(form,formInModal) {
    var inputs =  $(form).find('input');
    var vendor = '';
    if (formInModal) {
        vendor = $("#select_vendor_dep").val();
    } else {
        vendor = $("#select_vendor").val();
    }

    for (i in inputs) {
        val = inputs[i].value;
        name = inputs[i].name;
        name = global.splitCamelCaseToString(name);
        if (name != undefined && val != undefined) {
            if (val.length == 0 || val == ' ' ) {
                if (vendor == 'openstack-nova' && !name.includes("region") && !name.includes("amaz")) {
                    swal("Error",name +" can not be empty!",'error');
                    return false;
                }
                if (vendor == 'aws-ec2' && !name.includes("open")) {
                    swal("Error",name +" can not be empty!",'error');
                    return false;
                }

            }
        }
    }
    return true;
}


$(function() {
    showCredentialsDiv($('#select_vendor').val());
    showCredentialsDivInModal($('#select_vendor_dep').val())
    onSubmitJsonFileForm();
    onSubmitAttachDeploymentForm();
    onSubmitSettingsForm();
});

function showCredentialsDivInModal(selectedValue){
   var os_div = $('#openstack_credentials_dep');
   var ec2_div = $('#amazon_credentials_dep');
   console.log(selectedValue)
   if(selectedValue ===  'openstack-nova') {
        $('#amz_img_id').hide();
        $('#os_img_id').show();
        os_div.show();
        ec2_div.hide();
   } else if (selectedValue === 'aws-ec2') {
        $('#amz_img_id').show();
        $('#os_img_id').hide();
        ec2_div.show();
        os_div.hide();
   } else {
        $('#amz_img_id').show();
        $('#os_img_id').hide();
        os_div.hide();
        ec2_div.hide();
   }
}

function showCredentialsDiv(selectedValue){
   var os_div = $('#openstack_credentials');
   var ec2_div = $('#amazon_credentials');
   var os_VM_div = $('#openstackVMSettings');
   var ec2_VM_div = $('#amazonVMSettings');
   global.clearEstimation();
   if(selectedValue ===  'openstack-nova') {
        os_div.show();
        os_VM_div.show();
        ec2_div.hide();
        ec2_VM_div.hide();
   } else if (selectedValue === 'aws-ec2') {
        ec2_div.show();
        ec2_VM_div.show();
        os_div.hide();
        os_VM_div.hide();
   } else {
        os_div.hide();
        os_VM_div.hide();
        ec2_div.hide();
        ec2_VM_div.hide();
   }
}

function analyzeLogs() {
    global.getTimeEstimation(true);
}

function onSubmitJsonFileForm() {
    $('#submitJsonFileForm').submit(function(event){
        event.preventDefault();
        $.ajax({
          url: $('#submitJsonFileForm').attr('action'),
          data : new FormData(this),
          cache: false,
          contentType: false,
          processData: false,
          type: "POST",
          success: function(){
            $('#loadJSONFileModal').modal('toggle');
            swal("Success!", "File loaded successfully!", "success").then(
                function() {
                    location.reload();
                });
          },
          error: function(data) {
            swal(data.responseJSON["error"],data.responseJSON["message"],'error');
          }
        });
        return false;
    });
}

function onSubmitAttachDeploymentForm() {
    $('#submitAttachmentForm').submit(function(event){
        event.preventDefault();
        if(validateSettingsForm(this,true)) {
            $.ajax({
              url: $('#submitAttachmentForm').attr('action'),
              data : new FormData(this),
              cache: false,
              contentType: false,
              processData: false,
              type: "POST",
              success: function(){
                location.href = "/status";
              },
              error: function(data) {
                swal(data.responseJSON["error"],data.responseJSON["message"],'error');
              }
            });
        }
        return false;
    });
}

function onSubmitSettingsForm() {
    $('#settings_form').submit(function(event){
        event.preventDefault();
        if (validateSettingsForm(this,false)) {
            $.ajax({
              url: $('#settings_form').attr('action'),
              data : new FormData(this),
              cache: false,
              contentType: false,
              processData: false,
              type: "POST",
              success: function(){
                location.href = "/status";
              },
              error: function(data) {
                swal(data.responseJSON["error"],data.responseJSON["message"],'error');
              }
            });
            return false;
        }
    });
}

function downloadJson() {
    $.ajax({
          url: "download/Json",
          cache: false,
          contentType: false,
          processData: false,
          type: "GET",
          success: function(){
            $("#estimator").append("<a id='download' href='/download/Json' style='display:hidden'></a>")
            document.getElementById("download").click()
          },
          error: function(data) {
            swal(data.responseJSON["error"],data.responseJSON["message"],'error');
          }
        });
}