var instances = [];
var estimations = [];
var dataTable;
var current_region;
var vendor;
var chart;

global = {
    plotEstimation: function (data) {
        getVendor().then(function(v) {
            if (v == 'openstack-nova') {
                vendor = 'openstack-nova';
                if (location.pathname != "/") {
                    getPricingFromOpenstack();
                }
            } else {
                vendor = 'aws-ec2';
                getPricingFromAmazon();
            }
            console.log("Vendor is",vendor);
        },function(err) { // in case of error, set vendor as openstack
            vendor = 'openstack-nova';
        })
        console.log(data);
        var labels = Object.keys(data[0]);
        var estimates = Object.values(data[0]);
        var scaledEstimates = Object.values(data[1]);
        var realRunEstimates = Object.values(data[2]);
        var data = {
            labels: labels,
            datasets: [
                {
                    label: "Time estimation",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: estimates
                },
                {
                    label: "Scaled estimation",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: scaledEstimates
                }
            ]
        };
        $("canvas").show();
        var ctx = document.getElementById('canvas').getContext('2d');
        var options = {
            animation: false,
            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines : true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            showLegend: true,
            scales : {
                yAxes: [{
                    scaleLabel : {
                        display: true,
                        labelString: "minutes"
                    }
                }],
                xAxes: [{
                    scaleLabel : {
                        display: true,
                        labelString: "cores"
                    }
                }]
            },
            showTooltips: true
        };
        chart = new Chart(ctx, {
            type: 'line',
            data: data,
            options: options
        });
        canvas = document.getElementById("canvas");
        canvas.addEventListener('click',global.onChartClick,false);
    },
    getTimeEstimation: function (analyzeFromUserLogs) {
        var valid = validateAnalyzeForm();
        console.log("Getting the time estimation!");
        $('#estimation_status').show();
        $('#estimation_status').html("Calculating the time estimation..");
        if (analyzeFromUserLogs && valid) {
            var transferLogFileLocation = document.getElementById("transferLogFileLocation").value;
            var jobsLogFileLocation = document.getElementById("jobsLogFileLocation").value;
            var DAXFileLocation = document.getElementById("daxFileLocation").value;
            var json = {"transferLogFileLocation": transferLogFileLocation, "jobsLogFileLocation": jobsLogFileLocation, "daxFileLocation": DAXFileLocation};
            $.ajax({
                type: "POST",
                url: '/estimator/estimateTimeFromUserLogs',
                data: JSON.stringify(json),
                dataType: "json",
                contentType: "application/json",
                success: function(data) {
                    console.log(data);
                    if (data.length <= 0 || Object.values(data[0]).length <= 0) {
                        $('#estimation_status').html("Problem during estimation..");
                    } else {
                        global.plotEstimation(data);
                        $('#estimation_status').empty();
                    }
                },
                error: function(data) {
                    $('#estimation_status').empty();
                    swal(data.responseJSON["error"],data.responseJSON["message"],'error');
                }
            });
        }
        if (!analyzeFromUserLogs && valid) {
            $.ajax({
                type: "GET",
                url: '/estimator/estimateTimeFromInstanceLogs',
                success: function(data) {
                    if (data.length <= 0 || Object.values(data[0]).length <= 0) {
                        $('#estimation_status').html("Problem during estimation..");
                    } else {
                        global.plotEstimation(data);
                        $('#estimation_status').empty();
                    }

                },
                error: function(data) {
                    $('#estimation_status').empty();
                    swal(data.responseJSON["error"],data.responseJSON["message"],'error');
                }
            });
        }

    },
    onChartClick: function (evt) {
        var vendor = ($("#select_vendor").val());
        if (location.pathname == "/" && (vendor == "openstack-nova" || vendor == '' || vendor == undefined)) {
            var activePoint = chart.getElementAtEvent(evt);
            console.log(activePoint);
        } else {
            var activePoint = chart.getElementAtEvent(evt);
            console.log(activePoint);
            if (activePoint[0] != undefined) {
                $("#pricingTable").show();
                console.log(activePoint);
                cores = chart.data.labels[activePoint[0]._index];
                time = chart.data.datasets[activePoint[0]._datasetIndex].data[activePoint[0]._index];
                showTable(cores,time);
            }
        }
    },
    clearEstimation: function () {
        clearDataTable();
        if ($("#pricingTable").is(":visible")) {
            $("#pricingTable").hide();
        }
        $("canvas").hide();
    },
    splitCamelCaseToString: function (s) {
        return s.split(/(?=[A-Z])/).join(' ').toLowerCase().replace('.'," ");
    }
}
function clearDataTable() {
    if (dataTable) {
        $("#pricingTable").empty();
        dataTable.destroy();
        dataTable = null;
    }
}

function modifyVmSettingsOnClick() {
    $('#pricingTable tbody').on('click', 'tr', function () {
        var data = dataTable.row( this ).data();
        var expanded = ($('#vm_settings').attr('aria-expanded'));
        if (confirm('Are you sure you want to select '+data[0])) {
            if (expanded == 'false' || expanded == undefined) {
                $('#vm_settings_toggle').click();
            }
            $("#instanceType").val(data[0]);
            $("#nrOfInstancesInput").val(data[1]);
            $("#nrOfInstancesOutput").val(data[1]);
            $(window).scrollTop($('#vm_settings').offset().top);
        }
    } );
}

function updateDeploymentOnClick() {
    $('#pricingTable tbody').on('click', 'tr', function () {
        var data = dataTable.row( this ).data();
        var expanded = ($('#vm_settings').attr('aria-expanded'));
        var nrOfInstances = data[1];
        var instanceType = data[0];
        var instance;
        swal({
          title: "New deployment",
          text: 'Are you sure you want to deploy '+nrOfInstances+' instances of type '+instanceType,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#8cd4f5",
          confirmButtonText: "Deploy!",
        }).then(function(){ //if confirm button is clicked
            console.log(data);
            for (i in instances) {
                if (instances[i].type == instanceType) {
                    instance = instances[i];
                    break;
                }
            }
            var VMSettings = {};
            console.log(vendor)

            VMSettings.instanceType = instance.type;
            VMSettings.minStorage = instance.disk;
            VMSettings.minRam = instance.ram;
            VMSettings.minCores = instance.cores;

            VMSettings.nrOfInstances = nrOfInstances;
            console.log(VMSettings);
            $.ajax({
                type: "POST",
                url: '/deployment/update',
                data: JSON.stringify(VMSettings),
                dataType: "json",
                contentType: "application/json",
                success: function(data) {
                    $("#deployment_status").text(data.status);
                    location.reload();
                }
            });
        });
    });



}

function showTable(cores,time) {
    clearDataTable();
    for (key in instances) {
        var instance = instances[key];
        if (cores % instance.cores == 0) {
            var nrOfInstances = (cores/instance.cores);
            var hours;

                if (time%60 == 0) {
                    hours = time/60;
                } else {
                    hours = (time-time%60)/60 + 1;
                }

            var totalCost = round(nrOfInstances*hours*instance.cost,2);
            if (nrOfInstances != 1) {
                if (vendor === 'aws-ec2') {
                    $('#pricingTable').append('<tr><td>'+instance.type+'</td><td>'+nrOfInstances+'</td><td>'+instance.cost+'</td><td>'+hours+'</td><td>'+totalCost+'</td></tr>');
                } else {
                    $('#cost_of_one').remove();
                    $('#total_cost').remove();
                    $('#pricingTable').append('<tr><td>'+instance.type+'</td><td>'+nrOfInstances+'</td><td>'+hours+'</td></tr>');
                }
            }
        }

    }
    dataTable = $('#pricingTable').DataTable( {
        "order": [[ 2, "asc" ]]
    });

    if (window.location.pathname === '/status') {
        updateDeploymentOnClick();
    } else {
        modifyVmSettingsOnClick();
    }

}

//This is called after getPricingFromAmazon() automatically;
function callback(cb) {
    map = cb;
    var regions = map.config.regions
    for (var i in regions) {
        var region = regions[i].region;
        if (region == current_region) {
            instanceTypes = regions[i].instanceTypes;
            for (var j in instanceTypes) {
                var region_instances = (instanceTypes[j].sizes)
                for (var k in region_instances) {
                    var instance = {};
                    instance.type = region_instances[k].size;
                    instance.cores = region_instances[k].vCPU;
                    instance.ram = region_instances[k].memoryGiB;
                    instance.cost = region_instances[k].valueColumns[0].prices.USD;
                    instances.push(instance);
                }
            }
        }
    }
}

function getPricingFromAmazon() {

    function getStaticPricingFromAmazon() {
        $.ajax({url: "http://a0.awsstatic.com/pricing/1/ec2/linux-od.min.js",dataType: 'jsonp',success: function(data) {}});
    }

    var region = $("#region").val();
    if (location.pathname == "/" &&  region != undefined && region != '') {
        current_region = region;
        getStaticPricingFromAmazon();
    } else {
        $.ajax({
            type: "GET",
            url: '/region',
            success: function(region) {
                current_region = region;
                getStaticPricingFromAmazon();
            },
            error: function(data) { //incase of error, set default region to us-east-1
                current_region = "us-east-1";
                $("#region").val("us-east-1");
                getStaticPricingFromAmazon();
            }
        });
    }
}

// Gets the instance flavors from openstack, while setting the price to zero.
function getPricingFromOpenstack() {
    $.ajax({
        type: "GET",
        url: '/openstack/flavors',
        success: function(flavors) {
            console.log(flavors)
            for (var i in flavors) {
                var flavor = flavors[i];
                instance = {};
                instance.type = flavor.name;
                instance.cores = flavor.vcpus;
                instance.ram = flavor.ram;
                instance.disk = flavor.disk;
                instance.cost = 0;
                instances.push(instance);
            }
        }
    });
}

function getVendor() {
    var vendor = $("#select_vendor");
    if (vendor == null || vendor.val() == "" || vendor.val() == undefined) {
        return $.ajax({
            type: "GET",
            url: '/vendor'
        });
    } else {
          return new Promise(function(resolve, reject) {
            return resolve(vendor.val());
          })
    }
}


function round(number, precision) {
    var factor = Math.pow(10, precision);
    var tempNumber = number * factor;
    var roundedTempNumber = Math.round(tempNumber);
    return roundedTempNumber / factor;
};

Chart.defaults.global.hover.onHover = function(x) {
    if (x.length > 0) {
        document.getElementById("canvas").style.cursor = "pointer";
    } else {
        document.getElementById("canvas").style.cursor = "default";
    }
};

function validateAnalyzeForm() {
    var inputs = $('#estimator').find('input');
    console.log(inputs)
    for (i in inputs) {
        val = inputs[i].value;
        name = inputs[i].name;
        name = global.splitCamelCaseToString(name);
        if (name != undefined && val != undefined) {
            if (val.length == 0 || val == ' ' ) {
                swal("Error",name +" can not be empty!",'error');
                return false;
            }
        }
    }
    return true;
}